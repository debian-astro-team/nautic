/*
         
    sun.h sun routines
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef SUN_H
#define SUN_H

#include <wx/wx.h>
#include <wx/defs.h>
#include <wx/dynarray.h>
#include "stars.h"
//#include "sofa.h"
//#include "sofam.h"
#include "fund_args.h"
#include "orbital_elements.h"
#include "delta_t.h"
#include "spa.h"
#include "starpos.h"


class Sun {

public:
	Sun();
	virtual ~Sun();
	double CalcDeclination(int Y,int Mo,int D,int hr,int min,int sec);
	double CalcCoAscention(int Y,int Mo,int D,int hr,int min,int sec);
	double GHAAries(int Y,int Mo,int D,int hr,int min,int sec);
	double CalcSunPos(float d);
	int GetDayofYear(int year, int month, int day);
	wxString whatconstel (double Ra,double dec);
	wxString stConst;
	double lonsun;
	double Ts;
	double declination;
	double GHAsun;
	double RA;
	double jd;
	double beta;
	double lamda;
	double prlx;
	Starpos plan;

private:

	double xs;
	double ys;


};

#endif // SUN_H
