/*
         
    Pos-dialog.h plotting sheet
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef _POSDIALOG_
#define _POSDIALOG_

#include <wx/wx.h>
#include <wx/defs.h>
#include <wx/file.h>
#include <wx/string.h>
#include <wx/dialog.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class Pos_Dialog
///////////////////////////////////////////////////////////////////////////////
class Pos_Dialog : public wxDialog
	

{
	//DECLARE_DYNAMIC_CLASS(Pos_Dialog )
    //DECLARE_EVENT_TABLE()
private:
	void DrawIntercept(wxPaintDC * dc);
	void DrawPosLine(wxPaintDC * dc, double azt, double intcpt,double Lat);
	wxPoint CalcPoint(double azt,double intcpt,double Lat);
	void DrawApex(wxPaintDC * dc, wxPoint pnt, double azt, double intcpt);
	wxPoint GetIntPoint();
	void WritePosition(wxPaintDC *dc);

	wxPoint center;
	wxRegion rgn[10];
	wxPoint point[10];
	wxPoint pos_point;
	double pix;
	double Dlat;
	double Dlon;
	int n;
	double Lon_d;
	double Lat_d;

	double * ptLat;


protected:
	virtual void OnPaint( wxPaintEvent& event );// { event.Skip(); }

public:
		Pos_Dialog();
		Pos_Dialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Plotting Plan"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(650,700), long style =  wxCAPTION|wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER);
		~Pos_Dialog();
		double longitud;
		double latitud;
		wxString stpath;
		

};

#endif //__noname__
