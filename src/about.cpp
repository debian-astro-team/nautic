/*
         
    about.cpp implementation of about dlg
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#include "about.h"

///////////////////////////////////////////////////////////////////////////

AboutDialog::AboutDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->Centre( wxBOTH );


	//connect
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( AboutDialog::OnPaint ) );
}

void AboutDialog::OnPaint(wxPaintEvent& event)
{
	wxPaintDC dc(this);
	wxColour bk(0,0,0);
	wxPen  pen;
	pen.SetColour(bk);
	dc.SetPen(pen);
	dc.DrawText(_("This programm was designed and implemented by"),20,20);
	dc.DrawText(_("-Giovanni Enas-"),130,40);
	dc.DrawText(_("with the aid of SOFA libraries, refering to the algorithms"),10,60);
	dc.DrawText(_("and solutions of -Steve Moshier's Ephemeris Program-"),10,75);
          dc.DrawText(_("and Shiva Iyer routines.  "),10,90);
	dc.DrawText(_("Plus the -Solar Position Algorithm (SPA)- for sun position."),10,105);
         	dc.DrawText(_("and is intended to be an aid for navigators and amateurs"),10,120);
	dc.DrawText(_("for the calculation of body's ephemeris and the observer"),10,135);
	dc.DrawText(_("position in Astro Navigation."),10,150);
	dc.DrawText(_("Version: 1.5-1"),150,180);
	dc.DrawText(_("Copyright (C) 2011  Giovanni Enas"),110,195);
          dc.DrawText(_("License:  GPLv3"),140,210);

}

AboutDialog::~AboutDialog()
{
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( AboutDialog::OnPaint ) );
}
