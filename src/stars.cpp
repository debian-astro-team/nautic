/*
         
    star.cpp star implementation file
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */


#include "stars.h"
#include <wx/arrimpl.cpp> // This is a magic incantation which must be done!

WX_DEFINE_OBJARRAY(wxFloatArray);


    #define RADEG       (180.0/PI)
    #define DEGRAD      (PI/180.0)
    #define sind(x)     sin((x)*DEGRAD)
    #define cosd(x)     cos((x)*DEGRAD)
    #define tand(x)     tan((x)*DEGRAD)
    #define asind(x)    (RADEG*asin(x))
    #define acosd(x)    (RADEG*acos(x))
    #define atand(x)    (RADEG*atan(x))
    #define atan2d(y,x) (RADEG*atan2((y),(x)))



float revstr( float x )
    {
        return  x - floor(x/360.0)*360.0;
    }

Stars::Stars()
{
	time = wxDateTime::Now();
	declin = 0;
	RA = 0;

	astronames.Add(_("fi eridani"));
	astronames.Add(_("alpha eridani"));
	astronames.Add(_("alpha crucis"));
	stars.Add(_("Acamar"));
	stars.Add(_("Achernar"));
	stars.Add(_("Acrux"));
	stars.Add(_("Adhara"));
	stars.Add(_("Aldebaran"));
	stars.Add(_("Alioth"));
	stars.Add(_("Alkaid"));
	stars.Add(_("Almak"));
	stars.Add(_("Al Nair"));
	stars.Add(_("Alnilam"));
	stars.Add(_("Alphard"));
	stars.Add(_("Alphecca"));
	stars.Add(_("Alpheratz"));
	stars.Add(_("Altair"));
	stars.Add(_("Ankaa"));
	stars.Add(_("Antares"));
	stars.Add(_("Arcturus"));
	stars.Add(_("Atria"));
	stars.Add(_("Avior"));
	stars.Add(_("Bellatrix"));
	stars.Add(_("Betelgeuse"));
	stars.Add(_("Canopus"));
	stars.Add(_("Capella"));
	stars.Add(_("Castor"));
	stars.Add(_("Cor Caroli"));
	stars.Add(_("Deneb"));
	stars.Add(_("Denebola"));
	stars.Add(_("Diphda"));
	stars.Add(_("Dubhe"));
	stars.Add(_("Elnath"));
	stars.Add(_("Eltanin"));
	stars.Add(_("Enif"));
	stars.Add(_("Fomalhaut"));
	stars.Add(_("Gacrux"));
	stars.Add(_("Gienah"));
	stars.Add(_("Hadar"));
	stars.Add(_("Hamal"));
	stars.Add(_("Kaus Australis"));
	stars.Add(_("Kochab"));
	stars.Add(_("Markab"));
	stars.Add(_("Menkar"));
	stars.Add(_("Menkent"));
	stars.Add(_("Merak"));
	stars.Add(_("Miraplacidus"));
	stars.Add(_("Mirfak"));
	stars.Add(_("Mizar"));
	stars.Add(_("Nunki"));
	stars.Add(_("Peacock"));
	stars.Add(_("Phact"));
	stars.Add(_("Polaris"));
	stars.Add(_("Pollux"));
	stars.Add(_("Procyon"));
	stars.Add(_("Ras Alhague"));
	stars.Add(_("Regulus"));
	stars.Add(_("Rigel"));
	stars.Add(_("Rigil Kentaurus"));
	stars.Add(_("Sabik"));
	stars.Add(_("Saiph"));
	stars.Add(_("Schedar"));
	stars.Add(_("Scheddi"));
	stars.Add(_("Shaula"));
	stars.Add(_("Sirius"));
	stars.Add(_("Spica"));
	stars.Add(_("Suhail"));
	stars.Add(_("Vega"));
	stars.Add(_("Zuben el Genubi"));

}

double Stars::CalcAriesSideralTime(int Y,int Mo,int D,int hr,int min,int sec)
{
	double djm0;
	double djm;
	iauCal2jd(Y, Mo,D,&djm0,&djm);///funzione sofa per calcolare anno Juliano
	double Jtime = djm0 + djm;

	double hourrate =15.041068664;
	double GAST = iauGst06a(Jtime,0.0,Jtime,0.0);//funzione sofa per calcolare Ts alle 0UTC data
	GMST = iauGmst06(Jtime,0.0,Jtime,0.0);//g. Mean Sideral time
	GMST = GMST * RADEG;
	GAST = GAST * RADEG;//Ts in gradi
	double dfrac =  hr+((min+(sec/60.0000))/60.0000);
	GAST = GAST + dfrac*hourrate;
	GMST = GMST + dfrac*hourrate;
	equinox = GAST - GMST ;


	Ts = GAST;
	Ts=fmod(Ts,360);
	if(Ts<0)
		Ts=360+Ts;

	return Ts;

}

double Stars::CalcVariation(int Y,int Mo,int D,int hr,int min,int sec)
{
	Starpos a;
	a.linenum = 67;
	if(starname == _("Acamar"))
		a.linenum = 1;
	if(starname == _("Achernar"))
		a.linenum = 2;
	if(starname == _("Acrux"))
		a.linenum = 3;
	if(starname == _("Adhara"))
		a.linenum = 4;
	if(starname == _("Aldebaran"))
		a.linenum = 5;
	if(starname == _("Alioth"))
		a.linenum = 6;
	if(starname == _("Alkaid"))
		a.linenum = 7;
	if(starname == _("Almak"))
		a.linenum = 8;
	if(starname == _("Al Nair"))
		a.linenum = 9;
	if(starname == _("Alnilam"))
		a.linenum = 10;
	if(starname == _("Alphard"))
		a.linenum = 11;
	if(starname == _("Alphecca"))
		a.linenum = 12;
	if(starname == _("Alpheratz"))
		a.linenum = 13;
	if(starname == _("Altair"))
		a.linenum = 14;
	if(starname == _("Ankaa"))
		a.linenum = 15;
	if(starname == _("Antares"))
		a.linenum = 16;
	if(starname == _("Arcturus"))
		a.linenum = 17;
	if(starname == _("Atria"))
		a.linenum = 18;
	if(starname == _("Avior"))
		a.linenum = 19;
	if(starname == _("Bellatrix"))
		a.linenum = 20;
	if(starname == _("Betelgeuse"))
		a.linenum = 21;
	if(starname == _("Canopus"))
		a.linenum = 22;
	if(starname == _("Capella"))
		a.linenum = 23;
	if(starname == _("Castor"))
		a.linenum = 24;
	if(starname == _("Cor Caroli"))
		a.linenum = 25;
	if(starname == _("Deneb"))
		a.linenum = 26;
	if(starname == _("Denebola"))
		a.linenum = 27;
	if(starname == _("Diphda"))
		a.linenum = 28;
	if(starname == _("Dubhe"))
		a.linenum = 29;
	if(starname == _("Elnath"))
		a.linenum = 30;
	if(starname == _("Eltanin"))
		a.linenum = 31;
	if(starname == _("Enif"))
		a.linenum = 32;
	if(starname == _("Fomalhaut"))
		a.linenum = 33;
	if(starname == _("Gacrux"))
		a.linenum = 34;
	if(starname == _("Gienah"))
		a.linenum = 35;
	if(starname == _("Hadar"))
		a.linenum = 36;
	if(starname == _("Hamal"))
		a.linenum = 37;
	if(starname == _("Kaus Australis"))
		a.linenum = 38;
	if(starname == _("Kochab"))
		a.linenum = 39;
	if(starname == _("Markab"))
		a.linenum = 40;
	if(starname == _("Menkar"))
		a.linenum = 41;
	if(starname == _("Menkent"))
		a.linenum = 42;
	if(starname == _("Merak"))
		a.linenum = 43;
	if(starname == _("Miraplacidus"))
		a.linenum = 44;
	if(starname == _("Mirfak"))
		a.linenum = 45;
	if(starname == _("Mizar"))
		a.linenum = 46;
	if(starname == _("Nunki"))
		a.linenum = 47;
	if(starname == _("Peacock"))
		a.linenum = 48;
	if(starname == _("Phact"))
		a.linenum = 49;
	if(starname == _("Polaris"))
		a.linenum = 50;
	if(starname == _("Pollux"))
		a.linenum = 51;
	if(starname == _("Procyon"))
		a.linenum = 52;
	if(starname == _("Ras Alhague"))
		a.linenum = 53;
	if(starname == _("Regulus"))
		a.linenum = 54;
	if(starname == _("Rigel"))
		a.linenum = 55;
	if(starname == _("Rigil Kentaurus"))
		a.linenum = 56;
	if(starname == _("Sabik"))
		a.linenum = 57;
	if(starname == _("Saiph"))
		a.linenum = 58;
	if(starname == _("Schedar"))
		a.linenum = 59;
	if(starname == _("Scheddi"))
		a.linenum = 60;
	if(starname == _("Shaula"))
		a.linenum = 61;
	if(starname == _("Sirius"))
		a.linenum = 62;
	if(starname == _("Spica"))
		a.linenum = 63;
	if(starname == _("Suhail"))
		a.linenum = 64;
	if(starname == _("Vega"))
		a.linenum = 65;
	if(starname == _("Zuben el Genubi"))
		a.linenum = 66;
	if(a.linenum<0 || a.linenum>66)
		exit(0);

	a.mainStar(Y,Mo,D,hr,min,sec);
	RA = a.RA;
	declin = a.Dec;
	constellation = wxString::FromAscii( a.constname);

	//////////////////////lamda and beta////////////////////////////////
	
	double x =  cosd(RA) * cosd(declin);
    double y =  sind(RA) * cosd(declin);
    double z =  sind(declin);

	struct julian_date tdb;
	calendar_to_julian_date(Y, Mo, D, &tdb);
	double ecl = ge2000a_mean_obliquity(&tdb);
	double sinlat,coslon;
	ecl = ecl * RADEG;
	
	double p[3];	
	double theta;
	double phi;

	double xecl = x;
	double yecl = y * cosd(ecl) - z * sind(ecl);
	double zecl = y * sind(ecl) + z * cosd(ecl);
	p[0] = xecl;
	p[1] = yecl;
	p[2] = zecl;
	iauC2s( p, &theta, &phi);
	
	lon = theta * RADEG;
	if(lon < 0)
		lon = lon + 360;
	lat = phi * RADEG;

	return declin;

}


Stars::~Stars()
{
}

