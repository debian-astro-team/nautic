/*
         
    un_known_star.cpp unknown star implementation
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#include "un_known_star.h"
#include <wx/arrimpl.cpp> // This is a magic incantation which must be done!

static char starnam[80] = {'s','t','n','a','m','\0'};
double PIg = 3.14159265358979323846;
double RTSI = 2.0626480624709635516e5; /* arc seconds per radian */
const char * pthname = "/usr/share/nautic/stnam";


	#define RADEG       (180.0/PIg)
    #define DEGRAD      (PIg/180.0)
    #define sind(x)     sin((x)*DEGRAD)
    #define cosd(x)     cos((x)*DEGRAD)
    #define tand(x)     tan((x)*DEGRAD)
    #define asind(x)    (RADEG*asin(x))
    #define acosd(x)    (RADEG*acos(x))
    #define atand(x)    (RADEG*atan(x))
    #define atan2d(y,x) (RADEG*atan2((y),(x)))

UnKnownStar::UnKnownStar()
{
}
struct star
	{
	char obname[32];	/* Object name (31 chars) */
	double epoch;		/* Epoch of coordinates */
	double ra;		/* Right Ascension, radians */
	double dec;		/* Declination, radians */
	double px;		/* Parallax, radians */
	double mura;		/* proper motion in R.A., rad/century */
	double mudec;		/* proper motion in Dec., rad/century */
	double v;		/* radial velocity, km/s */
	double equinox;		/* Epoch of equinox and ecliptic */
	double mag;		/* visual magnitude */
};


wxString UnKnownStar::calc_bounds(double alt, double azth, double SDT, double Lat)
{


	double RA, Dec, r, d, HA;
	wxString result;
	const char * star = "/usr/share/nautic/stnam";

	double xh = cosd(azth) * cosd(alt);
    double yh = sind(azth) * cosd(alt);
    double zh = sind(alt);

	double xhor = xh * sind(Lat) - zh * cosd(Lat);
    double yhor = yh;
    double zhor = xh * cosd(Lat) + zh * sind(Lat);

	HA  = atan2d( yhor, xhor ) - 180 ;
    Dec  = atan2d( zhor, sqrt(xhor*xhor+yhor*yhor) );
	RA = (SDT - HA);

	if(RA < 0)
		RA += 360;
	RA = RA / 15;
	if(RA > 24)
		RA -=24;



	int linenum = 66;
	double rh, rm,rs, dd, dm, ds, epoch, mura, mudec, v, px, mag, ra , dec;
	char obname[32];

	char s[128];
	const char * str;
	const char * starname;
	const char * constname;
	int i;

	FILE *f;// *fopen();
	char *res;
	strcpy(starnam,pthname);
	f = fopen(starnam, "r" );
	wxString strMess;
	strMess = wxString::FromAscii(star);
	strMess = _("Can't find file: ") + strMess;
	if( f == 0 )
		{
		if( wxMessageBox(strMess,_("error"))==wxYES);
			goto ext;
		}


	for( i=0; i<linenum; i++ )
	{

			res =fgets( s, 126, f );

						if( *s == '-' )
								result = _("star unknown");

			/* Read in the ASCII string data and name of the object*/
			str = strtok(s," ");
			epoch = atof(str);
			str = strtok(NULL," ");
			rh = atof(str);
			str = strtok(NULL," ");
			rm = atof(str);
			str = strtok(NULL," ");
			rs = atof(str);
			str = strtok(NULL," ");
			dd = atof(str);
			str = strtok(NULL," ");
			dm = atof(str);
			str = strtok(NULL," ");
			ds = atof(str);
			str = strtok(NULL," ");
			mura = atof(str);
			str = strtok(NULL," ");
			mudec = atof(str);
			str = strtok(NULL," ");
			v = atof(str);
			str = strtok(NULL," ");
			px = atof(str);
			str = strtok(NULL," ");
			mag = atof(str);
			str = strtok(NULL," ");
			strcpy(obname,str);
			constname =obname;

				/* read the right ascension */
				ra = rh +(rm +rs/60)/60;
							/* read the declination */
				if(dd < 0)
					{
						dd = dd * -1;
						dec = dd + ((dm + ds/60))/60;
						dec = -dec;
					}
				else
					dec = dd + ((dm + ds/60))/60;

		////////////////////////////////controllo ra and dec  ////////////////////////////////
		r =	ra ;
		d = dec ;


		if(RA < (r + 1) && RA > (r - 1) )
		{
			if(Dec < (d + 1) && Dec > (d - 1))
			{
				starname = constname;
				result = wxString::FromAscii(starname);
				goto end;
			}

		}
		else
			result = _("Star Unknown");
	}
end:
	fclose(f);
ext:
	return result;

}

UnKnownStar::~UnKnownStar()
{
}


