/*
         
    Plot_dialog.cpp implementation of plotting dialog
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */
#include "plot_dialog.h"
          //IMPLEMENT_DYNAMIC_CLASS( Plot_dialog,wxDialog )

#define AZIMUTH_SPAN 4

#define RADEG       (180.0/PI)
#define DEGRAD      (PI/180.0)
#define sind(x)     sin((x)*DEGRAD)
#define cosd(x)     cos((x)*DEGRAD)
#define tand(x)     tan((x)*DEGRAD)
#define asind(x)    (RADEG*asin(x))
#define acosd(x)    (RADEG*acos(x))
#define atand(x)    (RADEG*atan(x))
#define atan2d(y,x) (RADEG*atan2((y),(x)))





Plot_dialog::Plot_dialog()
{
}

Plot_dialog::Plot_dialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{

	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	wxColour colour(0,200,255);

	this->SetBackgroundColour(colour);
	this->SetSizer( bSizer2 );
	this->Layout();

	this->Centre( wxBOTH );
	
	centre_toppoint.x = 375;
	centre_toppoint.y = 10;
	north_toppoint = centre_toppoint;
	//////////////////cursors/////////////////////////////////////
	
	#ifdef __WXMSW__
	wxBitmap open_bitmap(open_hand_bits, 32, 32);
	wxBitmap open_mask_bitmap(open_hand_mask, 32, 32);
	wxBitmap close_bitmap(close_hand_bits, 32, 32);
	wxBitmap close_mask_bitmap(close_hand_mask, 32, 32);

	open_bitmap.SetMask(new wxMask(open_mask_bitmap));
	close_bitmap.SetMask(new wxMask(close_mask_bitmap));

	wxImage open_hand_image = open_bitmap.ConvertToImage();
	open_hand_image.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, 6);
	open_hand_image.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, 14);
	wxImage close_hand_image = close_bitmap.ConvertToImage();
	close_hand_image.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, 6);
	close_hand_image.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, 14);

	open_cursor = wxCursor(open_hand_image);
	close_cursor = wxCursor(close_hand_image);
	#else
	open_cursor = wxCursor(open_hand_bits, 32, 32,
	  6, 14, open_hand_mask,wxBLACK, wxWHITE);
	close_cursor = wxCursor(close_hand_bits, 32, 32,
	  6, 14, close_hand_mask,wxBLACK, wxWHITE);
	#endif

	// Connect Events
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( Plot_dialog::OnPaint ) );
	this->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( Plot_dialog::OnLeftMouseClick ) );
	this->Connect( wxEVT_MOTION, wxMouseEventHandler(Plot_dialog::OnMotion ) );
	this->Connect( wxEVT_LEFT_UP, wxMouseEventHandler( Plot_dialog::OnLeftMouseUp ) );
}

wxPoint Plot_dialog::Calc_draw_coordinate(double azimuth, double alt)
{
	wxPoint point;
	if(azimuth <= 90) point.x = east_toppoint.x - ((90 - azimuth)* AZIMUTH_SPAN ) -10;
	if(azimuth > 90 && azimuth < 145) point.x = east_toppoint.x + ((azimuth - 90)* AZIMUTH_SPAN ) -10;
	if(azimuth >=145 && azimuth <= 180)	point.x = south_toppoint.x - ((180 - azimuth)* AZIMUTH_SPAN) -10;
	if(azimuth > 180 && azimuth <= 225)	point.x = south_toppoint.x + ((azimuth - 180)* AZIMUTH_SPAN) -10;
	if(azimuth > 225 && azimuth <= 270)	point.x = west_toppoint.x - ((270 - azimuth)* AZIMUTH_SPAN) -10;
	if(azimuth > 270 && west_toppoint.x < 1080)	point.x = west_toppoint.x + ((azimuth - 270) * AZIMUTH_SPAN) -10;
	if(azimuth > 270 && west_toppoint.x > 1080)	point.x = north_toppoint.x - ((360 -azimuth) * AZIMUTH_SPAN) -10;
	point.y = north_bottompoint.y - (altitude * AZIMUTH_SPAN) -10;
	return point;
}

void Plot_dialog::OnPaint(wxPaintEvent& event)
{
	Stars star;
	////////////points coordinates////////////////////////////////////////////
	int x = north_toppoint.x;
	wxPoint centre_point(375,400);
	wxPoint sun_point;
	wxPoint planet_point;
	wxPoint star_point;
	wxPoint moon_point;
	wxString name;
	/////////////////////calculating the coordinates of the dc for rotation///////////////////////////////
	east_toppoint.x = north_toppoint.x +  (90 *AZIMUTH_SPAN);
	if(x > 745) east_toppoint.x = north_toppoint.x -  (270 *AZIMUTH_SPAN);
	if(x > -710 && x < 28) south_toppoint.x = north_toppoint.x + (180 *AZIMUTH_SPAN);
	if(x < 745 && x >= 29) south_toppoint.x = north_toppoint.x - (180 *AZIMUTH_SPAN);
	if(x > 745) south_toppoint.x = north_toppoint.x - (180 *AZIMUTH_SPAN);

	if(x < 370)	 west_toppoint.x = north_toppoint.x + (270 *AZIMUTH_SPAN);
	if(x > 370)  west_toppoint.x = north_toppoint.x - (90 *AZIMUTH_SPAN);

	north_bottompoint.x = north_toppoint.x;
	north_bottompoint.y = north_toppoint.y + 355;

	east_toppoint.y = north_toppoint.y;
	east_bottompoint.x = east_toppoint.x;
	east_bottompoint.y = east_toppoint.y + 355;

	south_toppoint.y = north_toppoint.y;
	south_bottompoint.x = south_toppoint.x ;
	south_bottompoint.y = south_toppoint.y + 355;

	west_toppoint.y = north_toppoint.y;
	west_bottompoint.x = west_toppoint.x ;
	west_bottompoint.y = west_toppoint.y + 355;


	wxPaintDC dc( this );
    PrepareDC( dc );
	wxPen oldpen = dc.GetPen();
	wxPen pen(*wxBLUE,1,wxLONG_DASH);
	wxPen pen_blue(*wxBLUE,1,wxLONG_DASH);
	wxColour colour(155,67,15);
	wxColour night_col(20,20,20);
	wxColour twilight(123,122,224);

	int a=star.stars.GetCount();
	Calc_sun_data(&azth,&altitude,&sun_gha);
	sun_alt = altitude;
	sun_point = Calc_draw_coordinate(azth, altitude);
	Draw_sun(&dc,sun_point,sun_alt);//draw sun

	Calc_moon_data(&azth,&altitude, &moon_tm);
	moon_point = Calc_draw_coordinate(azth, altitude);
	moon_alt = altitude;
	Draw_moon(&dc,moon_point,moon_alt,moon_tm);//draw moon
	///////////////changing the background colour/////////////////////////////////////
	if(sun_alt < -15) this->SetBackgroundColour(night_col);
	if(sun_alt > -15 && sun_alt < 0) this->SetBackgroundColour(twilight);
	if(sun_alt < 0)
	{
		Calc_planet_data(&azth,&altitude, 0,&name);				////planets
		planet_point = Calc_draw_coordinate(azth, altitude);
		Draw_planet(&dc,planet_point,altitude,name);
		Calc_planet_data(&azth,&altitude, 1,&name);
		planet_point = Calc_draw_coordinate(azth, altitude);
		Draw_planet(&dc,planet_point,altitude,name);
		Calc_planet_data(&azth,&altitude, 2,&name);
		planet_point = Calc_draw_coordinate(azth, altitude);
		Draw_planet(&dc,planet_point,altitude,name);
		Calc_planet_data(&azth,&altitude, 3,&name);
		planet_point = Calc_draw_coordinate(azth, altitude);
		Draw_planet(&dc,planet_point,altitude,name);
		for(int b=0;b<a;b++)
		{
			Calc_star_data(&azth, &altitude, b, &name);		///stars
			star_point = Calc_draw_coordinate(azth, altitude);
			Draw_star(&dc,star_point,altitude,name);
		}

	}
	////////////////////////////////////////drawing the cardinal lines//////////////////////////////
	wxBrush brush(colour);
	dc.SetBrush(brush);
	dc.SetPen(pen);
	dc.DrawLine(north_toppoint,north_bottompoint);
	Draw_alt(&dc,north_bottompoint);
	dc.DrawLine(east_toppoint,east_bottompoint);
	Draw_alt(&dc,east_bottompoint);
	dc.DrawLine(south_toppoint,south_bottompoint);
	Draw_alt(&dc,south_bottompoint);
	dc.DrawLine(west_toppoint,west_bottompoint);
	Draw_alt(&dc,west_bottompoint);
	/////////////drawing the brown earth/////////////////////////////////////////////
	pen.SetColour(colour);
	dc.SetPen(pen);
	int  width = 0;
	int  heigth = 0;
	this->GetSize(&width, &heigth);
    dc.DrawRectangle( 0, 360, width, heigth / 5 );
	/////////////drawing the azimuth indicators for sun and moon//////////////////////////////////////////
	dc.SetPen(pen_blue);
	dc.DrawLine(sun_point.x + 10, sun_point.y + 20, sun_point.x + 10 , sun_point.y + (sun_alt * AZIMUTH_SPAN) + 30 ) ;
	dc.DrawLine(moon_point.x + 10, moon_point.y + 10, moon_point.x + 10 , moon_point.y + (moon_alt * AZIMUTH_SPAN) + 30 ) ;
	dc.SetPen(oldpen);
	///////////////writing cardinal figures///////////////////////////////////////
	north_bottompoint.x = north_bottompoint.x - 4;
	dc.DrawText(wxT("N"),north_bottompoint);
	east_bottompoint.x = east_bottompoint.x - 4;
	dc.DrawText(wxT("E"),east_bottompoint);
	south_bottompoint.x = south_bottompoint.x - 4;
	dc.DrawText(wxT("S"),south_bottompoint);
	west_bottompoint.x = west_bottompoint.x - 4;
	dc.DrawText(wxT("W"),west_bottompoint);
	////////////////////compass drawing//////////////////////////////

	north_bottompoint.x = north_bottompoint.x + 4;
	wxPoint nor = north_bottompoint;
	Draw_compass(&dc,nor);
}

void Plot_dialog::OnLeftMouseClick(wxMouseEvent& event)
{
		///////////////set cursor//////////////////////////
		 
		this->SetCursor(close_cursor);

		int a, b;
		event.GetPosition(&a,&b);
		point_from_centre_span = a - north_toppoint.x;
}


void Plot_dialog::OnMotion(wxMouseEvent& event)
{
	
	this->SetCursor(open_cursor);
	int a,b;
	if(event.m_leftDown)
	{
		this->SetCursor(close_cursor);
		event.GetPosition(&a,&b);
		north_toppoint.x = a - point_from_centre_span;

		if(north_toppoint.x < -710)    north_toppoint.x = north_toppoint.x + 1450;
		if(north_toppoint.x > 1440)    north_toppoint.x = north_toppoint.x -1440;
	
		
		this->Refresh();

	}
}

void Plot_dialog::OnLeftMouseUp(wxMouseEvent& event)
{
	
	this->SetCursor(open_cursor);

	int a,b;
	event.GetPosition(&a,&b);
	
}

void Plot_dialog::Calc_sun_data(double * azimuth, double * alt, double * GHA)
{
		Sun mysun;
		mysun.GHAAries(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
		declination = mysun.declination;
		double Tm = mysun.GHAsun;
		Tm = fmod(Tm,360);
		LHA = Tm + longitud;
		LHA = fmod(LHA,360);
		Ts = mysun.Ts;
		RA = mysun.RA;
		lamda = mysun.lamda;
		beta = mysun.beta;
		parlax = mysun.prlx;
		bodyname = _("Sun");
		constell = mysun.stConst;
		smdiam = 0;
	///////////////////////calcolo azimuth e altezza/////////////////////////////
	double x = cosd(LHA) * cosd(declination);
    double y = sind(LHA) * cosd(declination);
    double z = sind(declination);

	double xhor = x * sind(latitud) - z * cosd(latitud);
    double yhor = y;
    double zhor = x * cosd(latitud) + z * sind(latitud);

	*azimuth  = atan2d( yhor, xhor ) + 180;
    *alt  = atan2d( zhor, sqrt(xhor*xhor+yhor*yhor) );
	*GHA = Tm;
	sun_gha = Tm;
}

void Plot_dialog::Draw_sun(wxPaintDC * dc, wxPoint point, double altitude)
{
	wxPen oldpen = dc->GetPen();
	wxPen pen(*wxBLACK,1,wxSOLID);
	wxPen pen_blue(*wxBLUE,1,wxLONG_DASH);
	dc->SetPen(pen);
	wxColour light_yellow(238,238,88);
	wxColour yellow(243,243,13);
	wxColour red(247,39,1);
	wxBrush ly_brush(light_yellow);
	wxBrush y_brush(yellow);
	wxBrush r_brush(red);
	if(altitude <= 0) goto end;
	if(altitude > 0 && altitude < 10) dc->SetBrush(r_brush);
	if(altitude > 10 && altitude < 20) dc->SetBrush(y_brush);
	if(altitude > 20 ) dc->SetBrush(ly_brush);
	dc->DrawEllipse(point.x ,point.y,20,20);

end:
	dc->SetPen(oldpen);
	
}

void Plot_dialog::Draw_moon(wxPaintDC * dc, wxPoint point, double altitude, double GHA)
{
	wxBitmap full_btm(full_moon_bits, 32, 32);
	wxBitmap full_mask(full_moon_mask, 32, 32);
	full_btm.SetMask(new wxMask(full_mask));
	wxBitmap moon_1(moon_1_bits, 32, 32);
	wxBitmap mask_1(moon_1_mask, 32, 32);
	moon_1.SetMask(new wxMask(mask_1));
	wxBitmap moon_half(moon_half_bits, 32, 32);
	wxBitmap mask_half(moon_half_mask, 32, 32);
	moon_half.SetMask(new wxMask(mask_half));
	wxBitmap moon_ebb(moon_ebb_bits, 32, 32);
	wxBitmap ebb_mask(moon_ebb_mask, 32, 32);
	moon_ebb.SetMask(new wxMask(ebb_mask));
	wxBitmap moon_ebb_C(moon_ebb_C_bits, 32, 32);
	wxBitmap ebb_mask_C(moon_ebb_C_mask, 32, 32);
	moon_ebb_C.SetMask(new wxMask(ebb_mask_C));
	wxBitmap moon_half_C(moon_half_C_bits, 32, 32);
	wxBitmap half_mask_C(moon_half_C_mask, 32, 32);
	moon_half_C.SetMask(new wxMask(half_mask_C));
	wxBitmap moon_last(moon_last_bits, 32, 32);
	wxBitmap last_mask(moon_last_mask, 32, 32);
	moon_last.SetMask(new wxMask(last_mask));
	wxBitmap moon_new(moon_new_bits, 32, 32);
	wxBitmap new_mask(moon_new_mask, 32, 32);
	moon_new.SetMask(new wxMask(new_mask));

	/////////////////difference of GHA moon and sun///////////for elongation////////////
	if(GHA < sun_gha)	GHA = GHA + 360;
	int diff = GHA - sun_gha;
	if(diff > 360)	diff = diff - 360;

	wxPen oldpen = dc->GetPen();
	
	if(altitude <= 0) goto end;

	point.x = point.x - 5;
	point.y = point.y - 5;

	if(diff <= 22 || diff >= 337)    /////new moon
{	
	dc->DrawBitmap(moon_new,point.x,point.y,true);
}
	if(diff >23 && diff < 67)	/////last slice of moon///
{
	dc->DrawBitmap(moon_last,point.x,point.y,true);
}
if(diff >=67 && diff < 112)	/////half moon decreasing
{
	dc->DrawBitmap(moon_half_C,point.x,point.y,true);
}
if(diff >=112 && diff < 157)////moon ebb decreasing
{
	dc->DrawBitmap(moon_ebb_C,point.x,point.y,true);
}
if(diff >=157 && diff < 202)///////////full moon////
{
	dc->DrawBitmap(full_btm,point.x,point.y,true);
}
if(diff >=202 && diff < 247)///moon ebb increasing
{
	dc->DrawBitmap(moon_ebb,point.x,point.y,true);
}
if(diff >=247 && diff < 292)///moon half increasing
{
	dc->DrawBitmap(moon_half,point.x,point.y,true);
}
if(diff >=292 && diff < 337)////first slice of moon
{
	dc->DrawBitmap(moon_1,point.x,point.y,true);
}

end:
	dc->SetPen(oldpen);
}

void Plot_dialog::Draw_planet(wxPaintDC * dc, wxPoint point, double altitude, wxString name)
{
	wxFont oldfont(wxSystemSettings::GetFont(wxSYS_DEVICE_DEFAULT_FONT));
	wxColour yellow(243,243,13);
	wxFont font(6,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	wxPen oldpen;
	wxPen pen(*wxBLACK,1,wxSOLID);
	dc->SetPen(pen);
	wxBrush y_brush(yellow);
	dc->SetBrush(y_brush);
	if(altitude <= 0) goto end;
	dc->DrawEllipse(point.x ,point.y + 3,6,6);
	dc->SetTextForeground(*wxWHITE);
	dc->SetFont(font);
	////////////names of planets////////////////////
	dc->DrawText(name,point.x + 10, point.y + 1);
	pen.SetColour(*wxBLACK);
	dc->SetFont(oldfont);
	dc->SetTextForeground(*wxBLACK);

end:
	dc->SetPen(oldpen);

}

void Plot_dialog::Draw_star(wxPaintDC*dc, wxPoint point, double altitude, wxString name)
{
	wxFont oldfont(wxSystemSettings::GetFont(wxSYS_DEVICE_DEFAULT_FONT));
	wxFont font(6,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	wxPen oldpen;
	wxPen pen(*wxBLACK,1,wxSOLID);
	dc->SetPen(pen);
	wxBrush y_brush(*wxWHITE);
	dc->SetBrush(y_brush);
	if(altitude <= 0) goto end;
	dc->DrawEllipse(point.x ,point.y + 3,3,3);
	dc->SetTextForeground(*wxWHITE);
	dc->SetFont(font);
	////////////////write names of stars///////////////////////
	dc->DrawText(name,point.x + 10, point.y + 1);
	pen.SetColour(*wxBLACK);
	dc->SetFont(oldfont);
	dc->SetTextForeground(*wxBLACK);

end:
	dc->SetPen(oldpen);
}

void Plot_dialog::Draw_compass(wxPaintDC * dc, wxPoint point)
{
	/////////////draw the compass on the bottom of the dialog/////////////////////
	wxPoint pt = point;
	wxPoint px = point;
	wxString str, str_1;
	wxFont oldfont(wxSystemSettings::GetFont(wxSYS_DEVICE_DEFAULT_FONT));
	wxFont font(6,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	wxPen oldpen;
	wxPen pen(*wxBLACK,1,wxSOLID);
	dc->SetPen(pen);
	dc->SetFont(font);
	for(int a = 0; a < 360; a++)
	{
		pt = point;
		px = point;
		pt.x = pt.x + (a * AZIMUTH_SPAN);
		px.x = px.x - (a * AZIMUTH_SPAN );
		if(a%10==0)
		{
			str = wxString::Format(wxT("%d"),a);
			str_1 = wxString::Format(wxT("%d"),360-a);
			dc->DrawLine(pt.x,pt.y + 14, pt.x, pt.y + 26);
			dc->DrawLine(px.x,px.y + 14, px.x, px.y + 26);
			if(a%90!=0 && a!=0){
			dc->DrawText(str, pt.x - 4, pt.y + 2);
			dc->DrawText(str_1, px.x - 4, px.y + 2);

			}
		}
		else
		{
		dc->DrawLine(pt.x,pt.y + 20, pt.x, pt.y + 26);
		dc->DrawLine(px.x,px.y + 20, px.x, px.y + 26);
		}
	}
	dc->SetFont(oldfont);

}

void Plot_dialog::Draw_alt(wxPaintDC*dc, wxPoint point)
{
	///////////write the altitude segments and figures on the cardinal lines/////////////////
	wxFont oldfont(wxSystemSettings::GetFont(wxSYS_DEVICE_DEFAULT_FONT));
	wxFont font(6,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	wxString str;
	
	dc->SetFont(font);
	wxPoint pt = point;
	for(int a = 0; a <= 90; a ++)
	{
		if(a%10==0){
		pt.y = point.y;
		pt.y = pt.y - (a * AZIMUTH_SPAN) - 5;
		dc->DrawLine(pt.x - 4, pt.y, pt.x + 4, pt.y);
		dc->SetTextForeground(*wxBLUE);
		str = wxString::Format(wxT("%d"),a);
		dc->DrawText(str, pt.x + 6, pt.y - 4);
		}
	}
	dc->SetFont(oldfont);
	dc->SetTextForeground(*wxBLACK);
}

void Plot_dialog::Calc_moon_data(double * azimuth, double *alt, double * GHA)
{
	int e;
	Sun mysun;
	Stars star;
	Starpos planet;

	mysun.GHAAries(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
	t = (dHour+(dMinute + (dSeconds/ 60.00)) / 60.00) * 15;
	loctime = t + longitud;

	planet.tlat = latitud;
	planet.tlong = longitud;
	planet.objnum = 0;
	e = planet.mainPlanet(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
	Ts = mysun.Ts;
	RA = planet.RA;
	moon_tm = Ts + planet.RA;
	moon_tm = fmod(moon_tm,360);
	LHA = moon_tm + longitud;
	LHA = fmod(LHA,360);
	lamda = planet.mlong;
	beta = planet.mlat;
	parlax = planet.prlx;
	smdiam = planet.sdiam;
	declination = planet.Dec;
	bodyname = _("Moon");
	constell = wxString::FromAscii(planet.constellation);
	smdiam = 0;
	///////////////////////calcolo azimuth e altezza/////////////////////////////
	double x = cosd(LHA) * cosd(declination);
    double y = sind(LHA) * cosd(declination);
    double z = sind(declination);

	double xhor = x * sind(latitud) - z * cosd(latitud);
    double yhor = y;
    double zhor = x * cosd(latitud) + z * sind(latitud);

	*azimuth  = atan2d( yhor, xhor ) + 180;
    *alt  = atan2d( zhor, sqrt(xhor*xhor+yhor*yhor) );
	*GHA = moon_tm;
}

void Plot_dialog::Calc_planet_data(double* azimuth, double* alt, int iChoice, wxString* name)
{
	int e;
	Sun mysun;
	Stars star;
	Starpos planet;
	mysun.GHAAries(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
	t = (dHour+(dMinute + (dSeconds/ 60.00)) / 60.00) * 15;
	loctime = t + longitud;
	
		if(iChoice==0)
		{
			planet.objnum = 1;
			e = planet.mainPlanet(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
			Ts = mysun.Ts;
			RA = planet.RA;
			double venus_tm = Ts + planet.RA;
			venus_tm = fmod(venus_tm,360);
			declination = planet.Dec;
			LHA = venus_tm + longitud;
			LHA = fmod(LHA,360);
			lamda = planet.plong;
			beta = planet.plat;
			parlax = 0;
			smdiam = 0;
			//_distance = planet.planet_distance;
			bodyname = wxString::FromAscii(planet.bodyname);
			constell = wxString::FromAscii(planet.constellation);
			*name = bodyname;
		}

		if(iChoice==1)
		{

			planet.objnum = 2;
			e = planet.mainPlanet(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
			Ts = mysun.Ts;
			RA = planet.RA;
			double mars_tm = Ts + planet.RA;
			mars_tm = fmod(mars_tm,360);
			declination = planet.Dec;
			LHA = mars_tm + longitud;
			LHA = fmod(LHA,360);
			lamda = planet.plong;
			beta = planet.plat;
			parlax = 0;
			smdiam = 0;
			//P_distance = planet.planet_distance;
			bodyname = wxString::FromAscii(planet.bodyname);
			constell = wxString::FromAscii(planet.constellation);
			*name = bodyname;
		}
		if(iChoice==2)
		{

			planet.objnum = 3;
			e = planet.mainPlanet(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
			Ts = mysun.Ts;
			RA = planet.RA;
			double jupiter_tm = Ts + planet.RA;
			jupiter_tm = fmod(jupiter_tm,360);
			declination = planet.Dec;
			LHA = jupiter_tm + longitud;
			LHA = fmod(LHA,360);
			lamda = planet.plong;
			parlax = 0;
			smdiam = 0;
			beta = planet.plat;
			bodyname = wxString::FromAscii(planet.bodyname);
			constell = wxString::FromAscii(planet.constellation);
			*name = bodyname;
		}
		if(iChoice==3)
		{

			planet.objnum = 4;
			e = planet.mainPlanet(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
			Ts = mysun.Ts;
			RA = planet.RA;
			double saturn_tm = Ts + planet.RA;
			saturn_tm = fmod(saturn_tm,360);
			declination = planet.Dec;
			LHA = saturn_tm + longitud;
			LHA = fmod(LHA,360);
			lamda = planet.plong;
			beta = planet.plat;
			parlax = 0;
			smdiam = 0;
			bodyname = wxString::FromAscii(planet.bodyname);
			constell = wxString::FromAscii(planet.constellation);
			*name = bodyname;
		}
	///////////////////////calcolo azimuth e altezza/////////////////////////////
	double x = cosd(LHA) * cosd(declination);
    double y = sind(LHA) * cosd(declination);
    double z = sind(declination);

	double xhor = x * sind(latitud) - z * cosd(latitud);
    double yhor = y;
    double zhor = x * cosd(latitud) + z * sind(latitud);

	*azimuth  = atan2d( yhor, xhor ) + 180;
    *alt  = atan2d( zhor, sqrt(xhor*xhor+yhor*yhor) );
}

void Plot_dialog::Calc_star_data(double* azimuth, double* alt, int a, wxString * name)
{
	Sun mysun;
	Stars star;
	Starpos planet;
	wxString str;
	mysun.GHAAries(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
	Ts =  mysun.Ts;


	t = (dHour+(dMinute + (dSeconds/ 60.00)) / 60.00) * 15;
	loctime = t + longitud;
	

			str = star.stars[a];
			star.starname = str;
			star.CalcVariation(dYear,dMonth,dDay,dHour,dMinute,dSeconds);
			RA = star.RA; //star.co_asc_rct[b];
			declination = star.declin; //star.dec[b];
			double GHA = Ts + RA;
			GHA = fmod(GHA,360);
			LHA = GHA + longitud;
			LHA = fmod(LHA,360);
			lamda = star.lon;
			beta = star.lat;
			parlax = 0;
			smdiam = 0;
			bodyname = star.starname;
			constell =star.constellation;
			*name = bodyname;
/////////////////////////prova altezza quadratica////////////////////////////////
			double x = cosd(LHA) * cosd(declination);
			double y = sind(LHA) * cosd(declination);
			double z = sind(declination);

			double xhor = x * sind(latitud) - z * cosd(latitud);
			double yhor = y;
			double zhor = x * cosd(latitud) + z * sind(latitud);

			*azimuth  = atan2d( yhor, xhor ) + 180;
			*alt  = atan2d( zhor, sqrt(xhor*xhor+yhor*yhor) );
}


Plot_dialog::~Plot_dialog()
{
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( Plot_dialog::OnPaint ) );
	this->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( Plot_dialog::OnLeftMouseClick ) );
	this->Disconnect( wxEVT_MOTION, wxMouseEventHandler(Plot_dialog::OnMotion ) );
	this->Disconnect( wxEVT_LEFT_UP, wxMouseEventHandler( Plot_dialog::OnLeftMouseUp ) );

}
