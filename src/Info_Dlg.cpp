/*
         
    Info_Dlg.cpp implementation of Info Dialog
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#include "Info_Dlg.h"

///////////////////////////////////////////////////////////////////////////

Info_Dlg::Info_Dlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWFRAME ) );

	//wxBoxSizer* bSizer2;
	//bSizer2 = new wxBoxSizer( wxVERTICAL );

	wxGridSizer* gSizer2;
	gSizer2 = new wxGridSizer( 18, 2, 0, 0 );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxT("Body's name		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	gSizer2->Add( m_staticText26, 0,wxALL, 15 );
	m_staticText26->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	gSizer2->Add( m_staticText1, 0,wxALL, 15 );
	m_staticText1->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	//gSizer2->Add( 0, 0, 1, wxEXPAND, 5 );
	//gSizer2->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticText27 = new wxStaticText( this, wxID_ANY, wxT("Date	    		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	gSizer2->Add( m_staticText27, 0,wxALL, 15 );
	m_staticText27->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	gSizer2->Add( m_staticText2, 0,wxALL, 15 );
	m_staticText2->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText271 = new wxStaticText(this, wxID_ANY, wxT("Time UT			:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText271->Wrap( -1 );
	gSizer2->Add( m_staticText271, 0, wxALL, 15 );
	m_staticText271->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	gSizer2->Add( m_staticText3, 0,wxALL, 15 );
	m_staticText3->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText272 = new wxStaticText( this, wxID_ANY, wxT("Local Time		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText272->Wrap( -1 );
	gSizer2->Add( m_staticText272, 0, wxALL, 15 );
	m_staticText272->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	gSizer2->Add( m_staticText4, 0,wxALL, 15 );
	m_staticText4->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText273 = new wxStaticText( this, wxID_ANY, wxT("Julian Date		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText273->Wrap( -1 );
	gSizer2->Add( m_staticText273, 0, wxALL, 15 );
	m_staticText273->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText5 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	gSizer2->Add( m_staticText5, 0,wxALL, 15 );
	m_staticText5->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText263 = new wxStaticText( this, wxID_ANY, wxT("GHA aries		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText263->Wrap( -1 );
	gSizer2->Add( m_staticText263, 0, wxALL, 15 );
	m_staticText263->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	gSizer2->Add( m_staticText6, 0,wxALL, 15 );
	m_staticText6->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText274 = new wxStaticText( this, wxID_ANY, wxT("SHA                    	:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText274->Wrap( -1 );
	gSizer2->Add( m_staticText274, 0, wxALL, 15 );
	m_staticText274->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	gSizer2->Add( m_staticText7, 0,wxALL, 15 );
	m_staticText7->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText275 = new wxStaticText( this, wxID_ANY, wxT("Declination		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText275->Wrap( -1 );
	gSizer2->Add( m_staticText275, 0, wxALL, 15 );
	m_staticText275->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	gSizer2->Add( m_staticText8, 0,wxALL, 15 );
	m_staticText8->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText276 = new wxStaticText( this, wxID_ANY, wxT("Ecliptic Longitude	:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText276->Wrap( -1 );
	gSizer2->Add( m_staticText276, 0, wxALL, 15 );
	m_staticText276->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText9 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	gSizer2->Add( m_staticText9, 0,wxALL, 15 );
	m_staticText9->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );


	m_staticText277 = new wxStaticText( this, wxID_ANY, wxT("Ecliptic Latitude	:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText277->Wrap( -1 );
	gSizer2->Add( m_staticText277, 0, wxALL, 15 );
	m_staticText277->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	gSizer2->Add( m_staticText10, 0,wxALL, 15 );
	m_staticText10->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText278 = new wxStaticText( this, wxID_ANY, wxT("GHA			:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText278->Wrap( -1 );
	gSizer2->Add( m_staticText278, 0, wxALL, 15 );
	m_staticText278->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	gSizer2->Add( m_staticText11, 0,wxALL, 15 );
	m_staticText11->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );


	m_staticText279 = new wxStaticText( this, wxID_ANY, wxT("LHA				:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText279->Wrap( -1 );
	gSizer2->Add( m_staticText279, 0, wxALL, 15 );
	m_staticText279->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	gSizer2->Add( m_staticText12, 0,wxALL, 15 );
	m_staticText12->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText280 = new wxStaticText( this, wxID_ANY, wxT("Semidiameter		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText280->Wrap( -1 );
	gSizer2->Add( m_staticText280, 0, wxALL, 15 );
	m_staticText280->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	gSizer2->Add( m_staticText13, 0,wxALL, 15 );
	m_staticText13->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText281 = new wxStaticText( this, wxID_ANY, wxT("Horizontal Parallax :"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText281->Wrap( -1 );
	gSizer2->Add( m_staticText281, 0, wxALL, 15 );
	m_staticText281->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText14 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	gSizer2->Add( m_staticText14, 0,wxALL, 15 );
	m_staticText14->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText282 = new wxStaticText( this, wxID_ANY, wxT("Altitude			:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText282->Wrap( -1 );
	gSizer2->Add( m_staticText282, 0, wxALL, 15 );
	m_staticText282->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	gSizer2->Add( m_staticText15, 0,wxALL, 15 );
	m_staticText15->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText283 = new wxStaticText( this, wxID_ANY, wxT("Azimuth			:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText283->Wrap( -1 );
	gSizer2->Add( m_staticText283, 0, wxALL, 15 );
	m_staticText283->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText16 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	gSizer2->Add( m_staticText16, 0,wxALL, 15 );
	m_staticText16->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText284 = new wxStaticText( this, wxID_ANY, wxT("Constellation		:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText284->Wrap( -1 );
	gSizer2->Add( m_staticText284, 0, wxALL, 15 );
	m_staticText284->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	m_staticText17 = new wxStaticText( this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	gSizer2->Add( m_staticText17, 0,wxALL, 15 );
	m_staticText17->SetFont( wxFont( 10, 74, 90, 90, false, wxT("Sans") ) );

	this->SetSizer( gSizer2 );
	this->Layout();

	this->Centre( wxBOTH );
	// Connect Events
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( Info_Dlg::OnPaintDlgInfo ) );
}

Info_Dlg::~Info_Dlg()
{
	// Disconnect Events
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( Info_Dlg::OnPaintDlgInfo ) );
}
