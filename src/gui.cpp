/*
         
    gui.cpp implementation of Graphical user interface
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#include "gui.h"

///////////////////////////////////////////////////////////////////////////
//MainDialogBase::MainDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )

MainDialogBase::MainDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	wxIcon * icon = new wxIcon(_("/usr/share/pixmaps/sext.xpm"),wxBITMAP_TYPE_XPM ,30,30);
	this->SetIcon(*icon);

	m_timer= new wxTimer();//il mio timer
///////////////////////////////////////////menu data/////////////////////////////////////////////////
m_menubar2 = new wxMenuBar( 0 );
	m_menu5 = new wxMenu();
	wxMenuItem* m_menuItem11;
	m_menuItem11 = new wxMenuItem( m_menu5, wxID_ANY, wxString( wxT("Save pos. line") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu5->Append( m_menuItem11 );

	wxMenuItem* m_menuItem12;
	m_menuItem12 = new wxMenuItem( m_menu5, wxID_ANY, wxString( wxT("Cancel pos. lines") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu5->Append( m_menuItem12 );

	m_menubar2->Append( m_menu5, wxT("Position lines") );

	m_menu6 = new wxMenu();
	wxMenuItem* m_menuItem14;
	m_menuItem14 = new wxMenuItem( m_menu6, wxID_ANY, wxString( wxT("calc. body's param") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu6->Append( m_menuItem14 );

	wxMenuItem* m_menuItem16;
	m_menuItem16 = new wxMenuItem( m_menu6, wxID_ANY, wxString( wxT("meridian passage") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu6->Append( m_menuItem16 );

	wxMenuItem* m_menuItem19;
	m_menuItem19 = new wxMenuItem( m_menu6, wxID_ANY, wxString( wxT("plotting") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu6->Append( m_menuItem19 );

	m_menubar2->Append( m_menu6, wxT("Calculations") );

	m_menu7 = new wxMenu();
	wxMenuItem* m_menuItem17;
	m_menuItem17 = new wxMenuItem( m_menu7, wxID_ANY, wxString( wxT("body's info") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu7->Append( m_menuItem17 );

	wxMenuItem* m_menuItem18;
	m_menuItem18 = new wxMenuItem( m_menu7, wxID_ANY, wxString( wxT("about") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu7->Append( m_menuItem18 );

	m_menubar2->Append( m_menu7, wxT("Info") );

	this->SetMenuBar( m_menubar2 );
///////////////////////////////////////////////////////////////////////////////////////////////////////
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	bSizer3->Add( 0, 20, 0, wxEXPAND, 5 );
	//////////////////////////AGGIUNTA DATA/////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////

	wxStaticBoxSizer* sbSizer31;
	sbSizer31 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Date:") ), wxVERTICAL );

	wxFlexGridSizer* fgSizer31;
	fgSizer31 = new wxFlexGridSizer( 1, 5, 0, 0 );
	fgSizer31->AddGrowableRow( 0 );
	fgSizer31->SetFlexibleDirection( wxHORIZONTAL );
	fgSizer31->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxString m_choice3Choices[] = { wxT("Sun"), wxT("Mon"), wxT("Tue"), wxT("Wed"), wxT("Thu"), wxT("Fri"), wxT("Sat"), wxT("Sun") };
	int m_choice3NChoices = sizeof( m_choice3Choices ) / sizeof( wxString );
	m_choice3 = new wxChoice( this, 6, wxDefaultPosition, wxSize( 70,25 ), m_choice3NChoices, m_choice3Choices, 0 );
	m_choice3->SetSelection( 0 );
	fgSizer31->Add( m_choice3, 0, wxALL, 5 );

	wxString m_choice4Choices[] = { wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9"), wxT("10"), wxT("11"), wxT("12"), wxT("13"), wxT("14"), wxT("15"), wxT("16"), wxT("17"), wxT("18"), wxT("19"), wxT("20"), wxT("21"), wxT("22"), wxT("23"), wxT("24"), wxT("25"), wxT("26"), wxT("27"), wxT("28"), wxT("29"), wxT("30"), wxT("31") };
	int m_choice4NChoices = sizeof( m_choice4Choices ) / sizeof( wxString );
	m_choice4 = new wxChoice( this, 7, wxDefaultPosition, wxSize( 50,25 ), m_choice4NChoices, m_choice4Choices, 0 );
	m_choice4->SetSelection( 0 );
	fgSizer31->Add( m_choice4, 0, wxALL, 5 );

	wxString m_choice5Choices[] = { wxT("Jan"), wxT("Feb"), wxT("Mar"), wxT("Apr"), wxT("May"), wxT("Jun"), wxT("Jul"), wxT("Aug"), wxT("Sep"), wxT("Oct"), wxT("Nov"), wxT("Dec") };
	int m_choice5NChoices = sizeof( m_choice5Choices ) / sizeof( wxString );
	m_choice5 = new wxChoice( this, 8, wxDefaultPosition, wxSize( 60,25 ), m_choice5NChoices, m_choice5Choices, 0 );
	m_choice5->SetSelection( 0 );
	fgSizer31->Add( m_choice5, 0, wxALL, 5 );

	wxString m_choice6Choices[] = {wxT("1999"),wxT("2000"),wxT("2001"),wxT("2002"),wxT("2003"),wxT("2004"),wxT("2005"),wxT("2006"),wxT("2007"),wxT("2008"), wxT("2009"),wxT("2010"), wxT("2011"), wxT("2012"), wxT("2013"), wxT("2014"), wxT("2015"), wxT("2016"), wxT("2017"), wxT("2018"), wxT("2019"), wxT("2020"), wxT("2021"), wxT("2022"), wxT("2023"), wxT("2024"), wxT("2025"), wxT("2026"), wxT("2027"), wxT("2028"), wxT("2029"), wxT("2030") };
	int m_choice6NChoices = sizeof( m_choice6Choices ) / sizeof( wxString );
	m_choice6 = new wxChoice( this, 9, wxDefaultPosition, wxSize( -1,25 ), m_choice6NChoices, m_choice6Choices, 0 );
	m_choice6->SetSelection( 0 );
	fgSizer31->Add( m_choice6, 0, wxALL, 5 );

	fgSizer31->Add( 15, 0, 1, wxEXPAND, 5 );
	sbSizer31->Add( fgSizer31, 0, wxEXPAND, 5 );
	bSizer3->Add( sbSizer31, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	////////////////////////////////FINE DATA//////////////////////////////////////////////////////////////

	wxStaticBoxSizer* sbSizer3;
	sbSizer3 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Estimated Position:") ), wxVERTICAL );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 0, 12, 0, 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	fgSizer3->Add( 0, 10, 1, wxEXPAND, 5 );
	fgSizer3->Add( 0, 10, 1, wxEXPAND, 5 );

	m_staticText105 = new wxStaticText( this, wxID_ANY, wxT("   Deg°"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText105->Wrap( -1 );
	fgSizer3->Add( m_staticText105, 0, wxALL, 5 );

	m_staticText106 = new wxStaticText( this, wxID_ANY, wxT("  Min'"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText106->Wrap( -1 );
	fgSizer3->Add( m_staticText106, 0, wxALL, 5 );

	m_staticText107 = new wxStaticText( this, wxID_ANY, wxT("   Secs''"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText107->Wrap( -1 );
	fgSizer3->Add( m_staticText107, 0, wxALL, 5 );

	fgSizer3->Add( 0, 10, 1, wxEXPAND, 5 );
	fgSizer3->Add( 0, 10, 1, wxEXPAND, 5 );
	fgSizer3->Add( 0, 10, 1, wxEXPAND, 5 );

	m_staticText1051 = new wxStaticText( this, wxID_ANY, wxT("   Deg°"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1051->Wrap( -1 );
	fgSizer3->Add( m_staticText1051, 0, wxALL, 5 );

	m_staticText1062 = new wxStaticText( this, wxID_ANY, wxT("  Min'"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1062->Wrap( -1 );
	fgSizer3->Add( m_staticText1062, 0, wxALL, 5 );

	m_staticText1071 = new wxStaticText( this, wxID_ANY, wxT("   Secs''"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1071->Wrap( -1 );
	fgSizer3->Add( m_staticText1071, 0, wxALL, 5 );

	fgSizer3->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticText109 = new wxStaticText( this, wxID_ANY, wxT("Latitude:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText109->Wrap( -1 );
	fgSizer3->Add( m_staticText109, 0, wxALL, 5 );

	wxString m_choice1Choices[] = { wxT("N"), wxT("S") };
	int m_choice1NChoices = sizeof( m_choice1Choices ) / sizeof( wxString );
	m_choice1 = new wxChoice( this, 10, wxDefaultPosition, wxSize( 50,25 ), m_choice1NChoices, m_choice1Choices, 0 );
	m_choice1->SetSelection( 0 );
	m_choice1->SetMinSize( wxSize( 50,25 ) );

	fgSizer3->Add( m_choice1, 0, wxALL, 0 );

	m_textCtrl11 = new wxTextCtrl( this, 11, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	fgSizer3->Add( m_textCtrl11, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 0 );

	m_textCtrl12 = new wxTextCtrl( this, 12, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl12->SetMaxLength(5);
	fgSizer3->Add( m_textCtrl12, 0, 0, 5 );

	m_textCtrl1 = new wxTextCtrl( this, 13, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl1->SetMaxLength( 2 );
	fgSizer3->Add( m_textCtrl1, 0, wxRIGHT|wxLEFT, 5 );

	fgSizer3->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticText111 = new wxStaticText( this, 14, wxT("Longitude:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText111->Wrap( -1 );
	fgSizer3->Add( m_staticText111, 0, wxALL, 5 );

	wxString m_choice11Choices[] = { wxT("E"), wxT("W") };
	int m_choice11NChoices = sizeof( m_choice11Choices ) / sizeof( wxString );
	m_choice11 = new wxChoice( this, 14, wxDefaultPosition, wxSize( 50,25 ), m_choice11NChoices, m_choice11Choices, 0 );
	m_choice11->SetSelection( 0 );
	m_choice11->SetMinSize( wxSize( 50,25 ) );

	fgSizer3->Add( m_choice11, 0, wxALL, 0 );

	m_textCtrl13 = new wxTextCtrl( this, 15, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	fgSizer3->Add( m_textCtrl13, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_textCtrl14 = new wxTextCtrl( this, 16, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl14->SetMaxLength(5);
	fgSizer3->Add( m_textCtrl14, 0, 0, 5 );

	m_textCtrl15 = new wxTextCtrl( this, 17, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	fgSizer3->Add( m_textCtrl15, 0, wxRIGHT|wxLEFT, 5 );

	sbSizer3->Add( fgSizer3, 0, 0, 5 );

	bSizer3->Add( sbSizer3, 0, wxALL|wxEXPAND, 5 );
	/////////////////////////////////////////BOX TEMPO///////////////////////////////////

	wxStaticBoxSizer* sbSizer4;
	sbSizer4 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Time:") ), wxVERTICAL );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 1, 12, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	fgSizer2->Add( 0, 10, 1, wxEXPAND, 5 );

	m_staticText151 = new wxStaticText( this, wxID_ANY, wxT("Hours"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText151->Wrap( -1 );
	fgSizer2->Add( m_staticText151, 0, wxALL, 5 );

	m_staticText171 = new wxStaticText( this, wxID_ANY, wxT("   mins"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText171->Wrap( -1 );
	fgSizer2->Add( m_staticText171, 0, wxALL, 5 );

	m_staticText16 = new wxStaticText( this, wxID_ANY, wxT("  secs"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	fgSizer2->Add( m_staticText16, 0, wxALL, 5 );

	m_button7 = new wxButton( this, 18, wxT("Start"), wxDefaultPosition, wxSize( 50,25 ), 0 );
	fgSizer2->Add( m_button7, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	fgSizer2->Add( 0, 10, 1, wxEXPAND, 5 );
	//fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Hours"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	fgSizer2->Add( m_staticText15, 0, wxALL, 5 );

	m_staticText17 = new wxStaticText( this, wxID_ANY, wxT("   mins"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	fgSizer2->Add( m_staticText17, 0, wxALL, 5 );

	m_staticText161 = new wxStaticText( this, wxID_ANY, wxT("  secs"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText161->Wrap( -1 );
	fgSizer2->Add( m_staticText161, 0, wxALL, 5 );

	m_button6 = new wxButton( this, 26, wxT("UTC"), wxDefaultPosition, wxSize( 50,25 ), 0 );
	fgSizer2->Add( m_button6, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );
	fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Time UTC:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	fgSizer2->Add( m_staticText8, 0, wxALL, 5 );

	m_textCtrl111 = new wxTextCtrl( this,19, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl111->SetMaxLength( 2 );
	fgSizer2->Add( m_textCtrl111, 0, 0, 5 );

	m_textCtrl112 = new wxTextCtrl( this,20, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl112->SetMaxLength( 2 );
	fgSizer2->Add( m_textCtrl112, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_textCtrl113 = new wxTextCtrl( this, 21, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl113->SetMaxLength( 2 );
	fgSizer2->Add( m_textCtrl113, 0, wxRIGHT, 5 );

	m_button8 = new wxButton( this, 22, wxT("Stop"), wxDefaultPosition, wxSize( 50,25 ), 0 );
	fgSizer2->Add( m_button8, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_staticText21 = new wxStaticText( this, wxID_ANY, wxT("Local time:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	fgSizer2->Add( m_staticText21, 0, wxALL, 5 );
	//fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );

	m_textCtrl1111 = new wxTextCtrl( this, 23, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl1111->SetMaxLength( 2 );
	fgSizer2->Add( m_textCtrl1111, 0, 0, 5 );

	m_textCtrl1112 = new wxTextCtrl( this, 24, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl1112->SetMaxLength( 2 );
	fgSizer2->Add( m_textCtrl1112, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_textCtrl1113 = new wxTextCtrl( this, 25, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl1113->SetMaxLength( 2 );

	fgSizer2->Add( m_textCtrl1113, 0, wxBOTTOM|wxRIGHT, 5 );

	m_button9 = new wxButton( this, 27, wxT("Local "), wxDefaultPosition, wxSize( 50,25 ), 0 );
	fgSizer2->Add( m_button9, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	sbSizer4->Add( fgSizer2, 0, wxEXPAND, 5 );

	bSizer3->Add( sbSizer4, 0, wxALL|wxEXPAND, 5 );
	///////////////////////////////OSSERVAZIONE////////////////////////////////////////////////
	wxStaticBoxSizer* sbSizer5;
	sbSizer5 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Observation:") ), wxVERTICAL );

	////////////////////////////////////////////new 7 apr 2012/////////////////////////////////////////////////////////////
	wxGridBagSizer* gbSizer3;
	gbSizer3 = new wxGridBagSizer( 0, 0 );
	gbSizer3->SetFlexibleDirection( wxBOTH );
	gbSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	gbSizer3->SetMinSize( wxSize( 100,10 ) );

	gbSizer3->Add( 100, 10, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sbSizer5->Add( gbSizer3, 0, wxEXPAND, 5 );

	wxGridBagSizer* gbSizer4;
	gbSizer4 = new wxGridBagSizer( 0, 0 );
	gbSizer4->SetFlexibleDirection( wxBOTH );
	gbSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	gbSizer4->SetMinSize( wxSize( 100,30 ) );

	m_staticText24 = new wxStaticText( this, wxID_ANY, wxT("Star name:       "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText24->Wrap( -1 );
	gbSizer4->Add( m_staticText24, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER, 5 );

	m_comboBox3 = new wxComboBox( this, 28, wxEmptyString, wxDefaultPosition, wxSize( 120,25 ), 0, NULL,  wxCB_READONLY );
	m_comboBox3->Append( wxT("Acamar") );
	m_comboBox3->Append( wxT("Achernar") );
	m_comboBox3->Append( wxT("Acrux") );
	m_comboBox3->Append( wxT("Adhara") );
	m_comboBox3->Append( wxT("Aldebaran") );
	m_comboBox3->Append( wxT("Alioth") );
	m_comboBox3->Append( wxT("Alkaid") );
	m_comboBox3->Append( wxT("Almak") );
	m_comboBox3->Append( wxT("Al Nair") );
	m_comboBox3->Append( wxT("Alnilam") );
	m_comboBox3->Append( wxT("Alphard") );
	m_comboBox3->Append( wxT("Alphecca") );
	m_comboBox3->Append( wxT("Alpheratz") );
		m_comboBox3->Append( wxT("Altair") );
		m_comboBox3->Append( wxT("Ankaa") );
		m_comboBox3->Append( wxT("Antares") );
		m_comboBox3->Append( wxT("Arcturus") );
		m_comboBox3->Append( wxT("Atria") );
		m_comboBox3->Append( wxT("Avior") );
		m_comboBox3->Append( wxT("Bellatrix") );
		m_comboBox3->Append( wxT("Betelgeuse") );
		m_comboBox3->Append( wxT("Canopo") );
		m_comboBox3->Append( wxT("Capella") );
		m_comboBox3->Append( wxT("Castor") );
		m_comboBox3->Append( wxT("Cor Caroli") );
		m_comboBox3->Append( wxT("Deneb") );
		m_comboBox3->Append( wxT("Denebola") );
		m_comboBox3->Append( wxT("Diphda") );
		m_comboBox3->Append( wxT("Dubhe") );
		m_comboBox3->Append( wxT("Elnath") );
		m_comboBox3->Append( wxT("Eltanin") );
		m_comboBox3->Append( wxT("Enif") );
		m_comboBox3->Append( wxT("Fomalhaut") );
		m_comboBox3->Append( wxT("Gracrux") );
		m_comboBox3->Append( wxT("Gienah") );
		m_comboBox3->Append( wxT("Hadar") );
		m_comboBox3->Append( wxT("Hamal") );
		m_comboBox3->Append( wxT("Kaus Australis") );
		m_comboBox3->Append( wxT("Kochab") );
		m_comboBox3->Append( wxT("Markab") );
		m_comboBox3->Append( wxT("Menkar") );
		m_comboBox3->Append( wxT("Menkent") );
		m_comboBox3->Append( wxT("Merak") );
		m_comboBox3->Append( wxT("Miraplacidus") );
		m_comboBox3->Append( wxT("Mirfak") );
		m_comboBox3->Append( wxT("Mizar") );
		m_comboBox3->Append( wxT("Nunki") );
		m_comboBox3->Append( wxT("Peacock") );
		m_comboBox3->Append( wxT("Phact") );
		m_comboBox3->Append( wxT("Polaris") );
		m_comboBox3->Append( wxT("Pollux") );
		m_comboBox3->Append( wxT("Procyon") );
		m_comboBox3->Append( wxT("Ras Alhague") );
		m_comboBox3->Append( wxT("Regulus") );
		m_comboBox3->Append( wxT("Rigel") );
		m_comboBox3->Append( wxT("Rigil Kentaurus") );
		m_comboBox3->Append( wxT("Sabik") );
		m_comboBox3->Append( wxT("Saiph") );
		m_comboBox3->Append( wxT("Schedar") );
		m_comboBox3->Append( wxT("Scheddi") );
		m_comboBox3->Append( wxT("Shaula") );
		m_comboBox3->Append( wxT("Sirius") );
		m_comboBox3->Append( wxT("Spica") );
		m_comboBox3->Append( wxT("Suhail") );
		m_comboBox3->Append( wxT("Vega") );
		m_comboBox3->Append( wxT("Zuben el Genubi") );
	gbSizer4->Add( m_comboBox3, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxTOP, 5 );

	gbSizer4->Add( 142, 10, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button1 = new wxButton( this, 29, wxT("Calc Altitude"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer4->Add( m_button1, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button11 = new wxButton( this, 30, wxT("info"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer4->Add( m_button11, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	sbSizer5->Add( gbSizer4, 0, wxEXPAND, 5 );

	wxGridBagSizer* gbSizer5;
	gbSizer5 = new wxGridBagSizer( 0, 0 );
	gbSizer5->SetFlexibleDirection( wxBOTH );
	gbSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer5->SetMinSize( wxSize( 100,10 ) );

	sbSizer5->Add( gbSizer5, 0, wxEXPAND, 5 );

	m_staticText25 = new wxStaticText( this, wxID_ANY, wxT("Solar System:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText25->Wrap( -1 );
	gbSizer5->Add( m_staticText25, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_comboBox31 = new wxComboBox( this, 31, wxEmptyString, wxDefaultPosition, wxSize( 120,25 ), 0, NULL,  wxCB_READONLY);
	m_comboBox31->Append( wxT("Sun") );
	m_comboBox31->Append( wxT("Moon") );
	m_comboBox31->Append( wxT("Venus") );
	m_comboBox31->Append( wxT("Mars") );
	m_comboBox31->Append( wxT("Jupiter") );
	m_comboBox31->Append( wxT("Saturn") );
	gbSizer5->Add( m_comboBox31, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxBOTTOM, 5 );

	m_staticText29 = new wxStaticText( this, wxID_ANY, wxT("    Observed Altitude:   "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	gbSizer5->Add( m_staticText29, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl241 = new wxTextCtrl( this, 35, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxALIGN_RIGHT );
	m_textCtrl241->SetMaxLength( 2 );//obs altitude degrees
	gbSizer5->Add( m_textCtrl241, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer5->Add( 4, 10, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_textCtrl244 = new wxTextCtrl( this, 36, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxALIGN_LEFT );
	m_textCtrl244->SetMaxLength( 3 );// altitude minutes
	gbSizer5->Add( m_textCtrl244, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ),wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer5->Add( 4, 10, wxGBPosition( 0, 6 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_textCtrl243 = new wxTextCtrl( this, 37, wxEmptyString,  wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl243->SetMaxLength( 2 );// altitude seconds
	gbSizer5->Add( m_textCtrl243, wxGBPosition( 0, 7 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer5->Add( 4, 10, wxGBPosition( 0, 8 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_checkBox1 = new wxCheckBox( this, 38, wxT("upper limb"),  wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer5->Add( m_checkBox1, wxGBPosition( 0, 9 ), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxBOTTOM, 5 );
	//upper limb*/
/////////////////////////altra linea////////////////////////////
	wxGridBagSizer* gbSizer6;
	gbSizer6 = new wxGridBagSizer( 0, 0 );
	gbSizer6->SetFlexibleDirection( wxBOTH );
	gbSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer6->SetMinSize( wxSize( 100,10 ) );

	m_staticText28 = new wxStaticText( this, wxID_ANY, wxT("Calculated Alt: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	gbSizer6->Add( m_staticText28, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl24 = new wxTextCtrl( this, 32, wxEmptyString, wxDefaultPosition, wxSize( 100,25 ), wxTE_READONLY|wxTE_RIGHT );
	gbSizer6->Add( m_textCtrl24, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );//altitude

	gbSizer6->Add( 85, 10, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText301 = new wxStaticText( this, wxID_ANY, wxT("Index error:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText301->Wrap( -1 );
	gbSizer6->Add( m_staticText301, wxGBPosition( 0, 3), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl2421 = new wxTextCtrl( this, 39, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ), wxTE_RIGHT );
	m_textCtrl2421->SetMaxLength( 4 );//index error
	gbSizer6->Add( m_textCtrl2421, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer6->Add( 30, 10, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Eye height:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	gbSizer6->Add( m_staticText10, wxGBPosition( 0, 6), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl245 = new wxTextCtrl( this, 40, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_RIGHT );
	m_textCtrl245->SetMaxLength( 4 );//eye height
	gbSizer6->Add( m_textCtrl245, wxGBPosition( 0, 7 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("  mt."), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	gbSizer6->Add( m_staticText7, wxGBPosition( 0, 8), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	sbSizer5->Add( gbSizer6, 0, wxEXPAND, 5 );
///////////////altra linea//////////////////////////////
	wxGridBagSizer* gbSizer7;
	gbSizer7 = new wxGridBagSizer( 0, 0 );
	gbSizer7->SetFlexibleDirection( wxBOTH );
	gbSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer7->SetMinSize( wxSize( 100,0 ) );

	gbSizer7->Add( 30, 4, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sbSizer5->Add( gbSizer7, 0, wxEXPAND, 5 );
/////////altra linea/////////////////////////////////////
	wxGridBagSizer* gbSizer8;
	gbSizer8 = new wxGridBagSizer( 0, 0 );
	gbSizer8->SetFlexibleDirection( wxBOTH );
	gbSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer8->SetMinSize( wxSize( 100,10 ) );

	m_staticText30 = new wxStaticText( this, wxID_ANY, wxT("Azimuth:          "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText30->Wrap( -1 );
	gbSizer8->Add( m_staticText30, wxGBPosition( 0, 0), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl242 = new wxTextCtrl( this, 33, wxEmptyString, wxDefaultPosition, wxSize( 100,25 ), wxTE_RIGHT );
	gbSizer8->Add( m_textCtrl242, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );//azimuth

	gbSizer8->Add( 99, 4, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText9 = new wxStaticText( this, wxID_ANY, wxT("Intercept:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	gbSizer8->Add( m_staticText9, wxGBPosition( 0, 3), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl246 = new wxTextCtrl( this, 41, wxEmptyString, wxDefaultPosition, wxSize( 50,25 ),wxTE_READONLY|wxTE_RIGHT );
	m_textCtrl246->SetMaxLength( 4 );//intercept
	gbSizer8->Add( m_textCtrl246, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("  Miles"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	gbSizer8->Add( m_staticText10, wxGBPosition( 0, 5), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	sbSizer5->Add( gbSizer8, 0, wxEXPAND, 5 );
////////////////////////////////altra linea/////////////////////////////////////////////
	wxGridBagSizer* gbSizer9;
	gbSizer9 = new wxGridBagSizer( 0, 0 );
	gbSizer9->SetFlexibleDirection( wxBOTH );
	gbSizer9->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer9->SetMinSize( wxSize( 100,0 ) );

	gbSizer9->Add( 30, 4, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sbSizer5->Add( gbSizer9, 0, wxEXPAND, 5 );

//////////////////////////////////////altra linea/////////////////////////////////////////
	wxGridBagSizer* gbSizer10;
	gbSizer10 = new wxGridBagSizer( 0, 0 );
	gbSizer10->SetFlexibleDirection( wxBOTH );
	gbSizer10->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer10->SetMinSize( wxSize( 100,10 ) );

	m_staticText211 = new wxStaticText( this, wxID_ANY, wxT("Star unknown: "), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	m_staticText211->Wrap( -1 );
	gbSizer10->Add( m_staticText211, wxGBPosition( 0, 0), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl24211 = new wxTextCtrl( this,34, wxT("No match"), wxDefaultPosition, wxSize( 100,25 ),wxTE_READONLY|wxTE_RIGHT );
	gbSizer10->Add( m_textCtrl24211, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );//unknown star

	gbSizer10->Add( 75, 4, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("True Altitude:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	gbSizer10->Add( m_staticText8, wxGBPosition( 0, 3), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	//true altitude
	m_textCtrl247 = new wxTextCtrl( this, 42, wxEmptyString, wxDefaultPosition, wxSize( 100,25 ),wxTE_READONLY|wxTE_RIGHT );
	gbSizer10->Add( m_textCtrl247, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer10->Add( 15, 4, wxGBPosition( 0, 5), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button4 = new wxButton( this, 43, wxT("Save"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer10->Add( m_button4, wxGBPosition( 0, 6), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	sbSizer5->Add( gbSizer10, 0, wxEXPAND, 5 );


	bSizer3->Add( sbSizer5, 0, wxALL|wxEXPAND, 5 );

/////////////////////////////////////POSIZIONE///////////////////////////////////////////////
	wxStaticBoxSizer* sbSizer7;
	sbSizer7 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Position:") ), wxVERTICAL );
//////////////////prima linea///////////////////////////
	wxGridBagSizer* gbSizer11;
	gbSizer11 = new wxGridBagSizer( 0, 0 );
	gbSizer11->SetFlexibleDirection( wxBOTH );
	gbSizer11->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer11->SetMinSize( wxSize( 10,10 ) );

	gbSizer11->Add( 50, 10, wxGBPosition( 0,1), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sbSizer7->Add( gbSizer11, 0, wxEXPAND, 5 );
//////////////////////////////////////////////seconda linea///////////////////////////////////////////////
	wxGridBagSizer* gbSizer12;
	gbSizer12 = new wxGridBagSizer( 0, 0 );
	gbSizer12->SetFlexibleDirection( wxBOTH );
	gbSizer12->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer12->SetMinSize( wxSize( 0,0 ) );


	m_button411 = new wxButton( this, 46, wxT("Calc Position"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer12->Add( m_button411, wxGBPosition( 0, 0), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer12->Add( 50, 10, wxGBPosition( 0,1), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText1091 = new wxStaticText( this, wxID_ANY, wxT("Latitude : "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1091->Wrap( -1 );
	gbSizer12->Add( m_staticText1091, wxGBPosition( 0, 2), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	m_textCtrl2422 = new wxTextCtrl( this, 47, wxEmptyString, wxDefaultPosition, wxSize( 100,25 ), wxTE_RIGHT );
	gbSizer12->Add( m_textCtrl2422, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	gbSizer12->Add( 50, 2, wxGBPosition( 0,4), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText1111 = new wxStaticText( this, wxID_ANY, wxT("Longitude: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1111->Wrap( -1 );
	gbSizer12->Add( m_staticText1111, wxGBPosition( 0, 5), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	gbSizer12->Add( 5, 10, wxGBPosition( 0,6), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_textCtrl24221 = new wxTextCtrl( this, 48, wxEmptyString, wxDefaultPosition, wxSize( 100,25 ), wxTE_RIGHT );
	gbSizer12->Add( m_textCtrl24221, wxGBPosition( 0, 7 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );


	sbSizer7->Add( gbSizer12, 0, wxEXPAND, 5 );
//////////////////////////////////////////TERZA LINEA/////////////////////////////////////
	wxGridBagSizer* gbSizer13;
	gbSizer13 = new wxGridBagSizer( 0, 0 );
	gbSizer13->SetFlexibleDirection( wxBOTH );
	gbSizer13->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer13->SetMinSize( wxSize( 0,0 ) );

	m_button40 = new wxButton( this, 45, wxT("Meridian"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer13->Add( m_button40, wxGBPosition( 0, 0), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );
	///////////////14 apr 2012 aggiunte, da togliere a fine lavori///////////////////////
	gbSizer13->Add( 50, 2, wxGBPosition( 0,1), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_staticText22 = new wxStaticText( this, wxID_ANY, wxT("Stars above the horizon at current position : "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText22->Wrap( -1 );
	gbSizer13->Add( m_staticText22, wxGBPosition( 0, 2), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxTOP, 5 );

	gbSizer13->Add( 4, 10, wxGBPosition( 0,3), wxGBSpan( 1, 1 ), wxEXPAND, 5 );
	//////////////added april 7///////////////////////////////////////////////////////
	m_button43 = new wxButton( this, 46, wxT("Sky view"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer13->Add( m_button43, wxGBPosition( 0, 4), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxCENTER, 5 );
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sbSizer7->Add( gbSizer13, 0, wxEXPAND, 5 );
/////////////////////////////////////QUARTA LINEA/////////////////////////////////
	wxGridBagSizer* gbSizer14;
	gbSizer14 = new wxGridBagSizer( 0, 0 );
	gbSizer14->SetFlexibleDirection( wxBOTH );
	gbSizer14->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	gbSizer14->SetMinSize( wxSize( 100,10 ) );

	m_button41 = new wxButton( this, 44, wxT("Cancel Saved"), wxDefaultPosition, wxSize( 100,25 ), 0 );
	gbSizer14->Add( m_button41, wxGBPosition( 0, 0), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxCENTER, 5 );

	sbSizer7->Add( gbSizer14, 0, wxEXPAND, 5 );

	bSizer3->Add( sbSizer7, 0, wxEXPAND|wxALL, 5 );

	this->SetSizer( bSizer3 );
	this->Layout();
	// Connect Events
	/////////////////////////////////////EVENTI/////////////////////////////////////////////////////////
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainDialogBase::OnCloseDialog ) );
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( MainDialogBase::OnPaint ) );
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( MainDialogBase::OnInitDialog ));
	this->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( MainDialogBase::OnKeyDown ) );
	m_textCtrl12->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnChar ), NULL, this );
	m_textCtrl11->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDeg ), NULL, this );
	m_textCtrl1->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharSec ), NULL, this );
	m_textCtrl13->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLon ), NULL, this );
	m_textCtrl14->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDegLon ), NULL, this );
	m_textCtrl15->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharSecLon ), NULL, this );
	m_textCtrl245->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharEHt ), NULL, this );
	m_textCtrl12->Connect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( MainDialogBase::OnTextMax ), NULL, this );
	m_textCtrl11->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_Deg_Lat ), NULL, this );
	m_textCtrl12->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_min_Lat ), NULL, this );
	m_textCtrl1->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_Sec_Lat ), NULL, this );
	m_textCtrl13->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextDegLon ), NULL, this );
	m_textCtrl14->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextMinLon ), NULL, this );
	m_textCtrl15->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextSecLon ), NULL, this );
	m_button7->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnStartTimer ), NULL, this );
	m_button8->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnStopTimer ), NULL, this );
	m_textCtrl111->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnHrsUTC ), NULL, this );
	m_textCtrl112->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnMinUTC ), NULL, this );
	m_textCtrl113->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnSecUTC ), NULL, this );
	m_textCtrl1111->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalHr ), NULL, this );
	m_textCtrl1112->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalMin ), NULL, this );
	m_textCtrl1113->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalSec ), NULL, this );
	m_textCtrl1111->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalHr ), NULL, this );
	m_textCtrl1112->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalMin ), NULL, this );
	m_textCtrl1113->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLocalSec ), NULL, this );
	m_textCtrl111->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTChrs ), NULL, this );
	m_textCtrl112->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTCmin ), NULL, this );
	m_textCtrl113->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTCsecs ), NULL, this );
	m_textCtrl1111->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTHrs ), NULL, this );
	m_textCtrl1112->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTmins ), NULL, this );
	m_textCtrl1113->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTsecs ), NULL, this );
	m_button6->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnUTC ), NULL, this );
	m_button9->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnLocal ), NULL, this );
	//m_comboBox3->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainDialogBase::OnStarChoice ), NULL, this );
	m_button1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcHeitht ), NULL, this );
	//m_comboBox31->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainDialogBase::OnPlanetChoice ), NULL, this );
	//m_button11->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcSolarHt ), NULL, this );
	m_textCtrl2421->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnTextEnter ), NULL, this );
	m_textCtrl244->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnAltMin ), NULL, this );
	m_textCtrl243->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnAltSec ), NULL, this );
	m_textCtrl245->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnEyeHt ), NULL, this );
	m_textCtrl2421->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDiff ), NULL, this );
	m_textCtrl241->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAlt ), NULL, this );
	m_textCtrl244->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAltMin ), NULL, this );
	m_textCtrl243->Connect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAltSec ), NULL, this );
	m_textCtrl241->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainDialogBase::OnTextAlt ), NULL, this );
	m_textCtrl241->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnFocusOut ), NULL, this );
	m_button11->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnInfo ), NULL, this );
	m_checkBox1->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainDialogBase::OnCheckbox ), NULL, this );
	m_textCtrl242->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler(MainDialogBase::OnAzOut ), NULL, this );
	m_button4->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnSave ), NULL, this );
	m_button411->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcPos ), NULL, this );
	m_button41->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCancLines ), NULL, this );
	m_button40->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnMeridian ), NULL, this );
	m_button43->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnPlot ), NULL, this );
	///////////////menu///////////
	this->Connect( m_menuItem11->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuSave ) );
	this->Connect( m_menuItem12->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuCancel ) );
	this->Connect( m_menuItem14->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuAlt ) );
	this->Connect( m_menuItem16->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuMeridian ) );
	this->Connect( m_menuItem19->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuPlot ) );
	this->Connect( m_menuItem17->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuInfo ) );
	this->Connect( m_menuItem18->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuAbout ) );
	m_timer->Connect(wxEVT_TIMER,wxTimerEventHandler(MainDialogBase::OnTimer),NULL,this);
}
	
MainDialogBase::~MainDialogBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainDialogBase::OnCloseDialog ) );
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( MainDialogBase::OnPaint ) );
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( MainDialogBase::OnInitDialog ) );
	this->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( MainDialogBase::OnKeyDown ) );
	m_textCtrl12->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnChar ), NULL, this );
	m_textCtrl11->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDeg ), NULL, this );
	m_textCtrl1->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharSec ), NULL, this );
	m_textCtrl13->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharLon ), NULL, this );
	m_textCtrl14->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDegLon ), NULL, this );
	m_textCtrl15->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharSecLon ), NULL, this );
	m_textCtrl245->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharEHt ), NULL, this );
	m_textCtrl12->Disconnect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( MainDialogBase::OnTextMax ), NULL, this );
	m_textCtrl11->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_Deg_Lat ), NULL, this );
	m_textCtrl12->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_min_Lat ), NULL, this );
	m_textCtrl1->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnText_Sec_Lat ), NULL, this );
	m_textCtrl13->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextDegLon ), NULL, this );
	m_textCtrl14->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextMinLon ), NULL, this );
	m_textCtrl15->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextSecLon ), NULL, this );
	m_button7->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnStartTimer ), NULL, this );
	m_button8->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnStopTimer ), NULL, this );
	m_textCtrl111->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnHrsUTC ), NULL, this );
	m_textCtrl112->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnMinUTC ), NULL, this );
	m_textCtrl113->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnSecUTC ), NULL, this );
	m_textCtrl111->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTChrs ), NULL, this );
	m_textCtrl112->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTCmin ), NULL, this );
	m_textCtrl113->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextUTCsecs ), NULL, this );
	m_textCtrl1111->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTHrs ), NULL, this );
	m_textCtrl1112->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTmins ), NULL, this );
	m_textCtrl1113->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainDialogBase::OnTextLTsecs ), NULL, this );
	m_button6->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnUTC ), NULL, this );
	m_button9->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnLocal ), NULL, this );
	//m_comboBox3->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainDialogBase::OnStarChoice ), NULL, this );
	m_button1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcHeitht ), NULL, this );
	//m_comboBox31->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainDialogBase::OnPlanetChoice ), NULL, this );
	//m_button11->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcSolarHt ), NULL, this );

	m_textCtrl2421->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnTextEnter ), NULL, this );
	m_textCtrl244->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnAltMin), NULL, this );
	m_textCtrl243->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnAltSec ), NULL, this );
	m_textCtrl245->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnEyeHt ), NULL, this );
	m_textCtrl2421->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharDiff ), NULL, this );
	m_textCtrl241->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAlt ), NULL, this );
	m_textCtrl244->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAltMin ), NULL, this );
	m_textCtrl243->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainDialogBase::OnCharAltSec ), NULL, this );
	m_textCtrl241->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainDialogBase::OnTextAlt ), NULL, this );
	m_textCtrl241->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( MainDialogBase::OnFocusOut ), NULL, this );
	m_button11->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnInfo ), NULL, this );
	m_checkBox1->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainDialogBase::OnCheckbox ), NULL, this );
	m_textCtrl242->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler(MainDialogBase::OnAzOut ), NULL, this );
	m_button4->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnSave ), NULL, this );
	m_button411->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCalcPos ), NULL, this );
	m_button41->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnCancLines ), NULL, this );
	m_button40->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnMeridian), NULL, this );
	m_button43->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainDialogBase::OnPlot), NULL, this );
	///menu
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuSave ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuCancel ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuAlt ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuMeridian ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuPlot ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuInfo ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainDialogBase::OnMenuAbout ) );
	m_timer->Disconnect(wxEVT_TIMER,wxTimerEventHandler(MainDialogBase::OnTimer),NULL,this);

}
