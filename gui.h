///////////////////////////////////////////////////////////////////////////
/*
         
    gui.h - grafical interface
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */
///////////////////////////////////////////////////////////////////////////

#ifndef __gui__
#define __gui__

#include <wx/string.h>
#include <wx/choice.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/combobox.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/icon.h>
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/timer.h>
#include <wx/gbsizer.h>


///////////////////////////////////////////////////////////////////////////
//
//		MyFrame2( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
///////////////////////////////////////////////////////////////////////////////
/// Class MainDialogBase
///////////////////////////////////////////////////////////////////////////////
class MainDialogBase : public wxFrame
{
	private:

protected:
		wxMenuBar* m_menubar2;
		wxMenu* m_menu5;
		wxMenu* m_menu6;
		wxMenu* m_menu7;

		wxChoice* m_choice3;
		wxChoice* m_choice4;
		wxChoice* m_choice5;
		wxChoice* m_choice6;

		wxStaticText* m_staticText105;
		wxStaticText* m_staticText106;
		wxStaticText* m_staticText107;
		wxStaticText* m_staticText10;
		wxStaticText* m_staticText22;

		wxStaticText* m_staticText109;
		wxChoice* m_choice1;
		wxTextCtrl* m_textCtrl11;
		wxTextCtrl* m_textCtrl12;
		wxTextCtrl* m_textCtrl1;
		wxTextCtrl* m_textCtrl245;

		wxStaticText* m_staticText111;
		wxChoice* m_choice11;
		wxTextCtrl* m_textCtrl13;
		wxTextCtrl* m_textCtrl14;
		wxTextCtrl* m_textCtrl15;

		wxStaticText* m_staticText15;
		wxStaticText* m_staticText17;
		wxStaticText* m_staticText16;

		wxStaticText* m_staticText8;

		wxTextCtrl* m_textCtrl111;
		wxTextCtrl* m_textCtrl112;
		wxTextCtrl* m_textCtrl113;

		wxStaticText* m_staticText21;

		wxTextCtrl* m_textCtrl1111;
		wxTextCtrl* m_textCtrl1112;
		wxTextCtrl* m_textCtrl1113;

		wxStaticText* m_staticText24;
		wxStaticText* m_staticText7;
		wxComboBox* m_comboBox3;

		wxButton* m_button1;
		wxButton* m_button43;

		wxStaticText* m_staticText25;
		wxComboBox* m_comboBox31;

		wxButton* m_button11;

		wxStaticText* m_staticText28;
		wxTextCtrl* m_textCtrl24;
		wxTextCtrl* m_textCtrl243;
		wxTextCtrl* m_textCtrl244;
		wxTextCtrl* m_textCtrl246;
		wxTextCtrl* m_textCtrl247;
		wxStaticText* m_staticText29;
		wxTextCtrl* m_textCtrl241;

		wxStaticText* m_staticText1051;
		wxStaticText* m_staticText1062;
		wxStaticText* m_staticText1071;
		wxStaticText* m_staticText9;

		wxStaticText* m_staticText151;
		wxStaticText* m_staticText171;
		wxStaticText* m_staticText161;

		wxStaticText* m_staticText30;
		wxTextCtrl* m_textCtrl242;
		wxStaticText* m_staticText301;
		wxTextCtrl* m_textCtrl2421;
		wxStaticText* m_staticText211;
		wxTextCtrl* m_textCtrl24211;
		wxButton* m_button4;
		wxButton* m_button7;
		wxButton* m_button8;
		wxButton* m_button6;
		wxButton* m_button9;
		wxButton* m_button40;

		wxStaticText* m_staticText51;
		wxTextCtrl* m_textCtrl11111;
		wxButton* m_button41;
		wxButton* m_button411;

		wxStaticText* m_staticText1091;
		wxTextCtrl* m_textCtrl2422;
		wxStaticText* m_staticText1111;
		wxTextCtrl* m_textCtrl24221;
		wxCheckBox* m_checkBox1;


		// Virtual event handlers, overide them in your derived class
		virtual void OnCloseDialog( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnPaint( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnInitDialog( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void OnKeyDown( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnText_Deg_Lat( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnText_min_Lat( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnText_Sec_Lat( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChar( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharDeg( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharSec( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharLon( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharDegLon( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharSecLon( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharEHt( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnTextDegLon( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextMinLon( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextSecLon( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextMax( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStartTimer( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStopTimer( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnHrsUTC( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnMinUTC( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnSecUTC( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharLocalHr( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharLocalMin( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharLocalSec( wxKeyEvent& event ) { event.Skip(); }

		virtual void OnTextUTChrs( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextUTCmin( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextUTCsecs( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextLTHrs( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextLTmins( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextLTsecs( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLocal( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUTC( wxCommandEvent& event ) { event.Skip(); }
		//virtual void OnStarChoice( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCalcHeitht( wxCommandEvent& event ) { event.Skip(); }
		//virtual void OnPlanetChoice( wxCommandEvent& event ) { event.Skip(); }
		//virtual void OnCalcSolarHt( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextEnter( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnCharDiff( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharAlt( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnTextAlt( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnFocusOut( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnAltMin( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnAltSec( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnEyeHt( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnInfo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCharAltMin( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCharAltSec( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCheckbox( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAzOut( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnSave( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCalcPos( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancLines( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMeridian( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPlot( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateUI( wxUpdateUIEvent& event ) { event.Skip(); }
		//////////menu
		virtual void OnMenuSave( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuAlt( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuMeridian( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuPlot( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuInfo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMenuAbout( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTimer( wxTimerEvent& event ){ event.Skip(); }
		


public:


		MainDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Nautic Almanac"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(650,600),long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );// long style = wxCAPTION|wxCLOSE_BOX|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU );
		~MainDialogBase();
		wxTimer * m_timer;

};

#endif //__gui__
