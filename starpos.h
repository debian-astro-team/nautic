/*
         
    starpos.h star position
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */
#ifndef STARPOS_H
#define STARPOS_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "plant.h"



class Starpos {

public:
	Starpos();
	virtual ~Starpos();
	int mainStar(int Year, int month, int hour, int day, int min, int sec);
	int hms( double x );
	int trnsit(double J, double lha, double dec);
	void iter_func(double t, int (* func)());
	int update();
	int kepler(double J, struct orbit *e,double rect[],double polar[]);
	double sidrlt(double j,double tlong );
	int nutlo(double J);
	int epsiln(double J);
	int diurab(double last,double *ra,double *dec );
	int diurpx(double last,double *ra,double *dec,double dist );
	int showcor(char* strng,double p[],double dp[] );
	double refrac(double alt);
	int altaz(double pol[],double J );
	int dms(double x );
	int iter_trnsit( int (* func)() );
	int jtocal(double J );
	void mean_elements (double J);
	int sscc (int k,double arg,int n);
	int nutate(double J,double p[] );
	int g3plan (double J, plantbl *plan, double pobj[],int objnum);
	int gplan (double J, plantbl *plan,double pobj[]);
	double g1plan (double J, plantbl *plan);
	int g2plan (double J, plantbl *plan,double pobj[]);
	int gmoon (double J,double rect[],double pol[]);
	int embofs(double J,double ea[],double * pr );
	int precess( double R[],double J,int direction );
	double modtp(double x);
	double mod360(double x);
	int velearth(double J );
	int annuab(double p[] );
	double caltoj(long year,int month,double day );
	int deltap(double p0[],double p1[],double* dr,double * dd );
	int relativity(double p[],double q[],double e[] );
	int showrd(char* msg,double p[],double pol[] );
	int angles(double p[],double q[],double e[] );
	int fk4fk5(double p[],double m[], struct star *el );
	double deltat(double Y);
	int dostar ();
	int rstar(struct star *el);
	int kinit();
	int getorbit(struct orbit *el);
	FILE *fincat(char * name,int n,char * str1,char * str2 );
	int getnum(char* msg,void* num,const char* format );
	int getstar(struct star *el);
	int showcname( char *in );
	static int islow(char* p);
	static int skipwh(char *p);
	static int isnumber(char *p);
	static int isup(char *p);
	double zgetdate();
	double gethms();
	double zatan2(double x,double y );
	int mainPlanet(int Year,int month, int day, int hour, int minute, int second);
	int domoon();
	int doplanet();
	int reduce( struct orbit *elemnt,double q[],double e[] );
	int moonll(double J,double rect[],double pol[]);
	int lonlat(double pp[],double J,double polar[],int ofdate );
	int lightt( struct orbit *elemnt,double q[],double e[] );
	const char * whatconstel(double ra, double dec);// (double pp[],double epoch);

	double RA;
	double Dec;
	int linenum;
	int objnum;
	double planet_distance;

	double tlong;//-71.13;	/* Cambridge, Massachusetts */
	double tlat ;//42.38; /* geocentric */
	double glat;//42.17; /* geodetic */
	const char * constellation;
	const char * bodyname;
	const char * constname;
	double mlong;
	double mlat;
	double prlx;
	double sdiam;
	double plong;
	double plat;





};

#endif // STARPOS_H
