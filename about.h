//////////////////////////////////////////////////////////////////////////
//
 /*
  about.h - about dialog routines
 * Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 *
 * This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */
///////////////////////////////////////////////////////////////////////////

#ifndef __ABOUTDLG__
#define __ABOUTDLG__

#include <wx/wx.h>
#include <wx/defs.h>
#include <wx/file.h>
#include <wx/string.h>
#include <wx/dialog.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class AboutDialog
///////////////////////////////////////////////////////////////////////////////
class AboutDialog : public wxDialog
{
	private:

	protected:
	// Virtual event handlers, overide them in your derived class
		virtual void OnPaint( wxPaintEvent& event );

	public:

		AboutDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Nautic Almanac"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(400,250), long style = wxDEFAULT_DIALOG_STYLE );
		~AboutDialog();

};

#endif //__noname__
