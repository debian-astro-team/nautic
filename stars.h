/*
         
    stars.h star data
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef STARS_H
#define STARS_H


#include <wx/wx.h>
#include <wx/defs.h>
#include <wx/dynarray.h>
#include "sofa.h"
#include "sofam.h"
#include "sun.h"
#include "aberration.h"
#include "kepler.h"
#include "coordinate.h"
#include "nutation.h"
#include "precession.h"
#include "starpos.h"

WX_DECLARE_OBJARRAY(float,wxFloatArray);


class Stars {

public:
	Stars();
	virtual ~Stars();
	wxDateTime time;
	wxArrayString astronames;
	wxArrayString stars;
	
	double Ts;
	double co_rect;
	double declin;
	double CalcAriesSideralTime(int Y,int Mo,int D,int hr,int min,int sec);
	double CalcVariation(int Y,int Mo,int D,int hr,int min,int sec);

	double delta_ecl;
	double delta_lon;
	double GMST;
	double equinox;
	double delta_psi (double JDArg);
	double RA ;
	double Dec;
	double lon;
	double lat;
	wxString constellation;
	wxString starname;
private:
	
};





#endif // STARS_H
