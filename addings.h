/*
         
    addings.h - addings
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#define DEMO 0
#define TABSTART 1620
#define TABEND 2013
#define TABSIZ (TABEND - TABSTART + 1)

#define RTOH (12.0/PI)
#define mods3600(x) ((x) - 1.296e6 * floor ((x)/1.296e6))
#define mod3600(x) ((x) - 1296000. * floor ((x)/1296000.))

#define DISFAC 2.3454780e4
/* cosine of 90 degrees 50 minutes: */
#define COSSUN -0.014543897651582657
/* cosine of 90 degrees 34 minutes: */
#define COSZEN -9.8900378587411476e-3
#define PI  3.14159265358979323846
#define RTS  2.0626480624709635516e5

#define DTR  1.7453292519943295769e-2
#define RTD  5.7295779513082320877e1
 /* arc seconds per radian */
#define  STR  4.8481368110953599359e-6 /* radians per arc second */


#define  J2000  2451545.0	/* 2000 January 1.5 */
#define  B1950  2433282.423	/* 1950 January 0.923 Besselian epoch */
#define  J1900  2415020.0	/* 1900 January 0, 12h UT */




int kinit();
