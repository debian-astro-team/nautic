//////////////////////////////////////////////////////////////////////////
/*
         
    Info_Dlg.h - information dialog
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */
///////////////////////////////////////////////////////////////////////////

#ifndef __INFODLG__
#define __INFODLG__

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class Info_Dlg
///////////////////////////////////////////////////////////////////////////////
class Info_Dlg : public wxDialog
{
	private:

protected:
		wxStaticText* m_staticText26;
		wxStaticText* m_staticText27;
		wxStaticText* m_staticText271;
		wxStaticText* m_staticText272;
		wxStaticText* m_staticText273;
		wxStaticText* m_staticText274;
		wxStaticText* m_staticText275;
		wxStaticText* m_staticText276;
		wxStaticText* m_staticText277;
		wxStaticText* m_staticText278;
		wxStaticText* m_staticText279;
		wxStaticText* m_staticText280;
		wxStaticText* m_staticText281;
		wxStaticText* m_staticText282;
		wxStaticText* m_staticText283;
		wxStaticText* m_staticText284;
		wxStaticText* m_staticText263;
		wxStaticText* m_staticText1;
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText3;
		wxStaticText* m_staticText4;
		wxStaticText* m_staticText5;
		wxStaticText* m_staticText6;
		wxStaticText* m_staticText7;
		wxStaticText* m_staticText8;
		wxStaticText* m_staticText9;
		wxStaticText* m_staticText10;
		wxStaticText* m_staticText11;
		wxStaticText* m_staticText12;
		wxStaticText* m_staticText13;
		wxStaticText* m_staticText14;
		wxStaticText* m_staticText15;
		wxStaticText* m_staticText16;
		wxStaticText* m_staticText17;



	public:


		//Info_Dlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Body 's paramether"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,400 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL|wxFRAME_FLOAT_ON_PARENT );
		Info_Dlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Body's paramether"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 650,380 ), long style = wxCAPTION|wxDEFAULT_DIALOG_STYLE );
		~Info_Dlg();
protected:
	virtual void OnPaintDlgInfo( wxPaintEvent& event ) { event.Skip(); }

};

#endif //__noname__
