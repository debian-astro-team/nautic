/*
         
    Plot_dialog.h sky view
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef _PLOTDIALOG_
#define _PLOTDIALOG_


#include <wx/wx.h>
#include <wx/settings.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/dialog.h>
#include <wx/wfstream.h>
#include <wx/datstrm.h>
#include <wx/rawbmp.h>
#include <wx/file.h>
////////////////////////
#include "sun.h"
#include "stars.h"
#include "starpos.h"
#include "moon.h"



class Plot_dialog : public wxDialog {

          //DECLARE_DYNAMIC_CLASS(Plot_dialog )

public:
	Plot_dialog();
	Plot_dialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Stars  position"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(750,400), long style =  wxCAPTION|wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER);
	~Plot_dialog();
	////////////////////////////variable////////////////////////////////////////////
	double dYear ;
	double dMonth;
	double dDay;
	double dHour;
	double dMinute;
	double dSeconds;
	double latitud;
	double longitud;

protected:
		wxStaticBitmap* m_bitmap3;
		virtual void OnPaint( wxPaintEvent& event );// { event.Skip(); }
		virtual void OnLeftMouseClick( wxMouseEvent& event );
		virtual void OnMotion( wxMouseEvent& event );
		virtual void OnLeftMouseUp( wxMouseEvent& event );
		//variable
		wxPoint centre_toppoint;
		wxPoint centre_bottompoint;
		wxPoint east_toppoint;
		wxPoint east_bottompoint;
		wxPoint south_toppoint;
		wxPoint south_bottompoint;
		wxPoint north_toppoint;
		wxPoint north_bottompoint;
		wxPoint west_toppoint;
		wxPoint west_bottompoint;
		int point_from_centre_span;
private:
////////////////////function///////////////////////////////
		void Calc_sun_data(double* azimuth, double* alt, double *GHA);
		void Calc_moon_data(double* azimuth, double* alt, double *GHA);
		void Calc_planet_data(double* azimuth, double *alt, int iChoice, wxString* name);
		void Calc_star_data(double* azimuth, double* alt, int a, wxString* name);
		void Draw_sun(wxPaintDC * dc, wxPoint point, double altitude);
		void Draw_compass(wxPaintDC * dc, wxPoint point);
		void Draw_moon(wxPaintDC* dc, wxPoint point, double altitude,  double GHA);
		void Draw_planet(wxPaintDC*dc, wxPoint point, double altitude, wxString name);
		void Draw_star(wxPaintDC*dc, wxPoint point, double altitude, wxString name);
		void Draw_alt(wxPaintDC*dc, wxPoint point);
		wxPoint Calc_draw_coordinate(double azimuth, double alt);
		wxCursor open_cursor;
		wxCursor close_cursor;


//////////////////////variables/////////////////////////////
		double declination;
		double CoArect;
		double moon_alt;
		double moon_tm;
		double sun_alt;
		double sun_gha;
		//////////////tempo siderale/////////////////////////
		double Ts;
		double altitude;
		double azth;
		double RA;
		double LHA;
		double equinox;
		double loctime;
		double t;
		double lamda;
		double beta;
		double parlax;
		double smdiam;
		wxString bodyname;
		wxString constell;





};

#endif // PLOTDIALOG
