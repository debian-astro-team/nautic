/*
         
    un_known_star.h unknown star header file
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef UNKNOWNSTAR_H
#define UNKNOWNSTAR_H

#include <wx/wx.h>
#include <wx/defs.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

class UnKnownStar {

public:
	UnKnownStar();
	virtual ~UnKnownStar();
	wxString calc_bounds(double alt, double azth,double SDT,double Lat);
	int  getstar(struct star *el);
	FILE * fincat(char * name,int n,char * str1,char * str2 );

};

#endif // UNKNOWNSTAR_H
