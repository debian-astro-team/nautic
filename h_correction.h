/*
         
    h_correction.h - altitude correction functions and variables
    Copyright (C) 2011 Enas Giovanni <gio.enas@alice.it>
 
    This file is part of Nautic.

    Nautic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nautic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Nautic.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef HCORRECTION_H
#define HCORRECTION_H

#include <wx/wx.h>
#include <wx/defs.h>

class HCorrection {

public:
	HCorrection();
	virtual ~HCorrection();
	double CalcDip(double e);
	double CalcRefrac( double obs_h);
	double CalcSunRefrac( double obs_h);
	double moon_trd_coor(double px, double obs_h);
	double calc_sun_llimb(int mon);
	double calc_sun_uplimb(int mon);
	double moon_snd_coor(double obs_h);
	double moon_trd_up_coor(double px, double obs_h);
	double stars_corr(double e, double obs_h);
	double plan_corr(double e, double obs_h, double prlx);
	double sun_corr(double e, double obs_h, double mon, bool ch);
	double moon_corr(double e, double px, double obs_h, bool ch);
	int year;
	int month;
	int day;
	double prlax;

protected:
	double moon_altitude;

};

#endif // HCORRECTION_H
